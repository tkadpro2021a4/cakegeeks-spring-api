[![pipeline status](https://gitlab.com/tkadpro2021a4/cakegeeks-spring-api/badges/main/pipeline.svg)](https://gitlab.com/tkadpro2021a4/cakegeeks-spring-api/-/commits/main)
[![coverage report](https://gitlab.com/tkadpro2021a4/cakegeeks-spring-api/badges/main/coverage.svg)](https://gitlab.com/tkadpro2021a4/cakegeeks-spring-api/-/commits/main)

# CakeGeeks Spring API

CakeGeeks adalah sebuah projek website toko kue dimana pengguna dapat memesan kue, memilih topping sesuai keinginan, memantaui transaksi dari pembelian kue tersebut, dan melihat riwayat pembelian.

# Authors

1. [Astrid Diany Alwardy](https://gitlab.com/astriddiany)
2. [Ghifari Zakaria](https://gitlab.com/Ghifari.Z.R)
3. M. Azmie Alaudin
4. [Raniah Nur Hanami](https://gitlab.com/ranianurhanami)
5. [Zaki Indra](https://gitlab.com/zaki-indra)

#### Daftar Fitur Yang Akan Diimplementasikan
1. Authentication       : Zaki Indra
2. Membuat kue          : Astrid Diany Alwardy
3. Menambahkan topping  : Raniah Nur Hanami
4. Transaksi            : Ghifari Zakaria
5. History Transaksi    : M. Azmie Alaudin
