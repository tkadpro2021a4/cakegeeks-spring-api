package project.cakegeeks.auth.controller;

import project.cakegeeks.auth.converter.JwtAuthorizationFromHeader;
import project.cakegeeks.auth.core.exception.AuthenticationException;
import project.cakegeeks.auth.core.validator.LoginUserValidator;
import project.cakegeeks.auth.core.validator.RegisterUserValidator;
import project.cakegeeks.auth.service.UserServiceImpl;
import project.cakegeeks.auth.util.Jwt;
import project.cakegeeks.auth.util.JwtAuthorization;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = AuthController.class)
public class AuthControllerTest {
    private final String USERNAME = "budi77";
    private final String EMAIL = "budi@email.com";
    private final String PASSWORD = "Budi1oke!";
    private final String WRONG_PASSWORD = "Asiaa@@p123";

    private final String REGISTER_ERROR_MESSAGE = "PASSWORD_DOES_NOT_MATCH";
    private final String REGISTER_ERROR = "REGISTER_ERROR";
    private final String LOGIN_ERROR_MESSAGE = "WRONG_USERNAME_OR_PASSWORD";
    private final String LOGIN_ERROR = "LOGIN_ERROR";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserServiceImpl userService;

    @MockBean
    private Jwt jwt;

    @MockBean
    private JwtAuthorizationFromHeader jwtAuthorizationFromHeader;


    private LoginUserValidator loginUserValidator;
    private RegisterUserValidator registerUserValidator;

    @Test
    public void testControllerRegisterFailed() throws Exception {
        Map<String, Object> request = new HashMap<>();
        request.put("username", USERNAME);
        request.put("email", EMAIL);
        request.put("password", PASSWORD);
        request.put("verifyPassword", WRONG_PASSWORD);

        String json = new ObjectMapper().writeValueAsString(request);

        when(userService.register(any(RegisterUserValidator.class))).thenThrow(
                new AuthenticationException(REGISTER_ERROR_MESSAGE)
        );

        mockMvc.perform(post("/auth/register")
        .contentType(MediaType.APPLICATION_JSON)
        .content(json)
        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(handler().methodName("register"))
                .andExpect(jsonPath("$.error").value(REGISTER_ERROR))
                .andExpect(jsonPath("$.message").value(REGISTER_ERROR_MESSAGE));

    }

    @Test
    public void testControllerRegisterSuccess() throws Exception {
        registerUserValidator = new RegisterUserValidator(USERNAME, EMAIL, PASSWORD, PASSWORD);

        Map<String, Object> request = new HashMap<>();
        request.put("username", USERNAME);
        request.put("email", EMAIL);
        request.put("password", PASSWORD);
        request.put("verifyPassword", PASSWORD);

        Map<String, Object> serviceReturn = new HashMap<>();
        serviceReturn.put("username", USERNAME);
        serviceReturn.put("email", EMAIL);
        serviceReturn.put("hashedPassword", PASSWORD);

        when(userService.register(any(RegisterUserValidator.class))).thenReturn(serviceReturn);

        String json = new ObjectMapper().writeValueAsString(request);

        mockMvc.perform(post("/auth/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("register"))
                .andExpect(jsonPath("$.username").value(USERNAME))
                .andExpect(jsonPath("$.email").value(EMAIL))
                .andExpect(jsonPath("$.hashedPassword").value(PASSWORD));

    }

    @Test
    public void testControllerLoginFailed() throws Exception {
        Map<String, Object> request = new HashMap<>();
        request.put("username", USERNAME);
        request.put("password", WRONG_PASSWORD);

        String json = new ObjectMapper().writeValueAsString(request);

        when(userService.login(any(LoginUserValidator.class))).thenThrow(
                new AuthenticationException(LOGIN_ERROR_MESSAGE)
        );

        mockMvc.perform(post("/auth/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(handler().methodName("login"))
                .andExpect(jsonPath("$.error").value(LOGIN_ERROR))
                .andExpect(jsonPath("$.message").value(LOGIN_ERROR_MESSAGE));

    }

    @Test
    public void testControllerLoginSuccess() throws Exception {
        final String token = "mockjwttoken";
        registerUserValidator = new RegisterUserValidator(USERNAME, EMAIL, PASSWORD, PASSWORD);

        Map<String, Object> request = new HashMap<>();
        request.put("username", USERNAME);
        request.put("password", PASSWORD);

        String json = new ObjectMapper().writeValueAsString(request);

        Map<String, Object> serviceReturn = new HashMap<>();
        serviceReturn.put("Authorization", token);

        when(userService.login(any(LoginUserValidator.class))).thenReturn(serviceReturn);

        mockMvc.perform(post("/auth/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("login"))
                .andExpect(jsonPath("$.Authorization").value(token));
    }

    @Test
    public void testControllerAuthorizeSuccess() throws Exception {
        String secretKey = "ehehehehheheh21ghgd1hehehhehehehehheheh12131313313113ehehehehheheh@";
        Jwt thisJwt = new Jwt(secretKey);
        String token = thisJwt.createToken(USERNAME);

        String jwtToken = "Bearer " + token;
        JwtAuthorization jwtAuthorization = new JwtAuthorization(thisJwt);
        jwtAuthorization.authorize(jwtToken);

        when(jwtAuthorizationFromHeader.convert(any(String.class))).thenReturn(jwtAuthorization);

        mockMvc.perform(get("/auth/authorize")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", jwtToken)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("authorize"))
                .andExpect(jsonPath("$.username").value(USERNAME));
    }

}
