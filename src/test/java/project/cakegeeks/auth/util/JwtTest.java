package project.cakegeeks.auth.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import java.security.Key;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.junit.jupiter.api.Assertions.*;

public class JwtTest {

    private String SECRET_KEY = "makanpermen,permenlolipop,rasanelegiteddededed123lestkytss";
    private String USERNAME = "Budiandfriends";

    private static final long JWT_TOKEN_VALIDITY = 3 * 60 * 60 * 1000;

    @InjectMocks
    private Jwt jwt = new Jwt(SECRET_KEY);

    @Test
    public void testCreateJwtTokenReturnString() {
        String token = jwt.createToken(USERNAME);

        assertEquals(String.class, token.getClass());
    }

    @Test
    public void testGetUsernameFromToken() {
        String token = jwt.createToken(USERNAME);
        String username = jwt.getUsernameFromToken(token);

        assertEquals(USERNAME, username);
    }

    @Test
    public void testRefreshTokenReturnNewToken() throws InterruptedException {
        String token = jwt.createToken(USERNAME);
        TimeUnit.SECONDS.sleep(1);
        String newToken = jwt.refreshToken(token);

        assertNotEquals(token, newToken);
    }

    @Test
    public void testGetExipartionDate180Minute() {
        String token = jwt.createToken(USERNAME);

        Date now = new Date();
        Date oneHour = new Date(now.getTime() + JWT_TOKEN_VALIDITY);

        Date expirationDate = jwt.getExpirationDateFromToken(token);

        long diff = Math.abs(expirationDate.getTime() - oneHour.getTime());
        long diffHours = diff / (60 * 60 * 1000) % 24;

        assertEquals(0L, diffHours);
    }

    @Test
    public void testValidateToken() {
        String token = jwt.createToken(USERNAME);

        assertTrue(jwt.validateToken(token));

        Claims claims = Jwts.claims().setSubject(USERNAME);
        Date now = new Date();
        Date validity = new Date(now.getTime() - 60 * 1000);
        Key key = Keys.hmacShaKeyFor(SECRET_KEY.getBytes());

        String customToken = Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(validity)
                .signWith(key)
                .compact();

        assertFalse(jwt.validateToken(customToken));
    }


}
