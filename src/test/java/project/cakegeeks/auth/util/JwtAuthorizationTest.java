package project.cakegeeks.auth.util;

import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class JwtAuthorizationTest {
    private final String USERNAME = "budi77";
    private final String JWT_TOKEN = "Bearer mytoken";

    @Mock
    private Jwt jwt;

    @InjectMocks
    private JwtAuthorization jwtAuthorization;

    @BeforeEach
    public void setUp() {
        jwt = Mockito.mock(Jwt.class);
        jwtAuthorization = new JwtAuthorization(jwt);
    }

    @Test
    public void testGetResponseSuccess() {
        when(jwt.refreshToken(any(String.class))).thenReturn(JWT_TOKEN);

        Map<String, Object> response = jwtAuthorization.getResponse();

        // Step 1: Check size
        assertEquals(1, response.size());

        // Step 2: Check all field
        assertTrue(response.containsKey("Authorization"));
    }

    @Test
    public void testGetUsername() {
        when(jwt.getUsernameFromToken(any())).thenReturn(USERNAME);

        String result = jwtAuthorization.getUsername();

        assertEquals(USERNAME, result);
    }

    @Test
    public void testAuthorizeToken() throws Exception {
        when(jwt.validateToken(any(String.class))).thenReturn(true);

        jwtAuthorization.authorize(JWT_TOKEN);

        String result = jwtAuthorization.getJwtToken();
        assertEquals("mytoken", result);
    }
}
