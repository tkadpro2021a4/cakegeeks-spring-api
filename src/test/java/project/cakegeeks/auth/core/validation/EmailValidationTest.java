package project.cakegeeks.auth.core.validation;

import project.cakegeeks.auth.core.exception.AuthenticationException;
import project.cakegeeks.auth.core.validator.RegisterUserValidator;
import project.cakegeeks.auth.core.validator.UserValidator;

import java.lang.reflect.Field;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class EmailValidationTest {

    private final String VALID_EMAIL = "emailku@gmail.com";
    private final String INVALID_EMAIL = "email.kom";
    private final String VERY_LONG_EMAIL = "aasjhfsanfbjjnmxnmzxnnxqowuhwqoufu412431hqwufqoufgoqsjhbfjbqguofgu" +
            "wjellalsdokowj91829171jjdsjzhfkahg@email.com";
    private final String VERY_SHORT_EMAIL = "a@a.i";

    private final String USERNAME = "Budi";
    private final String PASSWORD = "Budi1oke!";
    private final String VERIFY_PASSWORD = "Budi1oke!";

    private UserValidator userValidator;

    @Test
    public void testSetterWorksProperly() throws NoSuchFieldException, IllegalAccessException {
        userValidator = new RegisterUserValidator(USERNAME, VALID_EMAIL, PASSWORD, VERIFY_PASSWORD);

        Validation tempValidation = new EmailValidation(userValidator);
        EmailValidation emailValidation = new EmailValidation(userValidator);

        emailValidation.setNextHandler(tempValidation);

        Field field = emailValidation.getClass().getDeclaredField("nextHandler");
        field.setAccessible(true);
        assertEquals(tempValidation, field.get(emailValidation));
    }

    @Test
    public void testEmailValidationUsingLongEmail() {
        userValidator = new RegisterUserValidator(USERNAME, VERY_LONG_EMAIL, PASSWORD, VERIFY_PASSWORD);
        EmailValidation emailValidation = new EmailValidation(userValidator);
        Exception exception = assertThrows(AuthenticationException.class,
                emailValidation::validate);

        String expectedMessage = "EMAIL_LENGTH_NOT_SUPPORTED";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void testEmailValidationUsingShortEmail() {
        userValidator = new RegisterUserValidator(USERNAME, VERY_SHORT_EMAIL, PASSWORD, VERIFY_PASSWORD);
        EmailValidation emailValidation = new EmailValidation(userValidator);
        Exception exception = assertThrows(AuthenticationException.class,
                emailValidation::validate);

        String expectedMessage = "EMAIL_LENGTH_NOT_SUPPORTED";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void testEmailValidationUsingInvalidEmail() {
        userValidator = new RegisterUserValidator(USERNAME, INVALID_EMAIL, PASSWORD, VERIFY_PASSWORD);
        EmailValidation emailValidation = new EmailValidation(userValidator);
        Exception exception = assertThrows(AuthenticationException.class,
            emailValidation::validate);

        String expectedMessage = "INVALID_EMAIL";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test()
    public void testEmailValidationUsingValidEmail() {
        userValidator = new RegisterUserValidator(USERNAME, VALID_EMAIL, PASSWORD, VERIFY_PASSWORD);
        EmailValidation emailValidation = new EmailValidation(userValidator);

        assertDoesNotThrow(emailValidation::validate);
    }
}
