package project.cakegeeks.auth.core.validation;

import project.cakegeeks.auth.core.exception.AuthenticationException;
import project.cakegeeks.auth.core.validator.RegisterUserValidator;
import project.cakegeeks.auth.core.validator.UserValidator;

import java.lang.reflect.Field;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PasswordValidationTest {
    private final String USERNAME = "budi";
    private final String EMAIL = "budi@email.com";

    private final String VALID_PASSWORD = "Budi1oke!";
    private final String INVALID_PASSWORD = "emailkom";
    private final String VERY_LONG_PASSWORD = "aasDFFhfsanfbjjnmxnmzxnnxqowuhwqoufu412431hqwufqoufgoqsjhbfjbqguofgu" +
            "wjellalsdokowj91829171jjdsjzhfkahg@sj!!";
    private final String VERY_SHORT_PASSWORD = "A@a.i11";

    private UserValidator userValidator;

    @Test
    public void testSetterWorksProperly() throws NoSuchFieldException, IllegalAccessException {
        userValidator = new RegisterUserValidator(USERNAME, EMAIL, VALID_PASSWORD, VALID_PASSWORD);

        Validation tempValidation = new EmailValidation(userValidator);
        PasswordValidation passwordValidation = new PasswordValidation(userValidator);

        passwordValidation.setNextHandler(tempValidation);

        Field field = passwordValidation.getClass().getDeclaredField("nextHandler");
        field.setAccessible(true);
        assertEquals(tempValidation, field.get(passwordValidation));
    }


    @Test
    public void testPasswordValidationUsingLongPassword() {
        userValidator = new RegisterUserValidator(USERNAME, EMAIL, VERY_LONG_PASSWORD, VERY_LONG_PASSWORD);
        PasswordValidation passwordValidation = new PasswordValidation(userValidator);
        Exception exception = assertThrows(AuthenticationException.class,
                passwordValidation::validate);

        String expectedMessage = "PASSWORD_CANNOT_BE_LONGER_THAN_50_CHARACTER";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void testPasswordValidationUsingShortPassword() {
        userValidator = new RegisterUserValidator(USERNAME, EMAIL, VERY_SHORT_PASSWORD, VERY_SHORT_PASSWORD);
        PasswordValidation passwordValidation = new PasswordValidation(userValidator);
        Exception exception = assertThrows(AuthenticationException.class,
                passwordValidation::validate);

        String expectedMessage = "PASSWORD_MUST_AT_LEAST_CONTAIN_8_CHARACTER" +
                "1_UPPERCASE_1_NUMBER_AND_1_SYMBOL";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void testPasswordValidationUsingInvalidPassword() {
        userValidator = new RegisterUserValidator(USERNAME, EMAIL, INVALID_PASSWORD, INVALID_PASSWORD);
        PasswordValidation passwordValidation = new PasswordValidation(userValidator);
        Exception exception = assertThrows(AuthenticationException.class,
                passwordValidation::validate);

        String expectedMessage = "PASSWORD_MUST_AT_LEAST_CONTAIN_8_CHARACTER" +
                "1_UPPERCASE_1_NUMBER_AND_1_SYMBOL";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test()
    public void testPasswordValidationUsingValidPassword() {
        userValidator = new RegisterUserValidator(USERNAME, EMAIL, VALID_PASSWORD, VALID_PASSWORD);
        PasswordValidation passwordValidation = new PasswordValidation(userValidator);

        assertDoesNotThrow(passwordValidation::validate);
    }

}
