package project.cakegeeks.auth.core.validation;

import project.cakegeeks.auth.core.exception.AuthenticationException;
import project.cakegeeks.auth.core.validator.RegisterUserValidator;
import project.cakegeeks.auth.core.validator.UserValidator;

import java.lang.reflect.Field;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class UsernameValidationTest {
    private final String VALID_USERNAME = "budimantap";
    private final String INVALID_USERNAME = "budimantap!";
    private final String VERY_LONG_USERNAME = "aasjhfsanfbjjnmxnmzxnnxqowuhwqoufu412431hqwufqoufgoqsjhbfjbqguofgu" +
            "wjellalsdokowj91829171jjdsjzhfkahgemailcom";
    private final String VERY_SHORT_USERNAME = "ab";

    private final String EMAIL = "budi@email.com";
    private final String PASSWORD = "Budi1oke!";
    private final String VERIFY_PASSWORD = "Budi1oke!";

    private UserValidator userValidator;

    @Test
    public void testSetterWorksProperly() throws NoSuchFieldException, IllegalAccessException {
        userValidator = new RegisterUserValidator(VALID_USERNAME, EMAIL, PASSWORD, VERIFY_PASSWORD);

        Validation tempValidation = new EmailValidation(userValidator);
        UsernameValidation usernameValidation = new UsernameValidation(userValidator);

        usernameValidation.setNextHandler(tempValidation);

        Field field = usernameValidation.getClass().getDeclaredField("nextHandler");
        field.setAccessible(true);
        assertEquals(tempValidation, field.get(usernameValidation));
    }


    @Test
    public void testUsernameValidationUsingLongUsername() {
        userValidator = new RegisterUserValidator(VERY_LONG_USERNAME, EMAIL, PASSWORD, VERIFY_PASSWORD);
        UsernameValidation usernameValidation = new UsernameValidation(userValidator);
        Exception exception = assertThrows(AuthenticationException.class,
                usernameValidation::validate);

        String expectedMessage = "USERNAME_LENGTH_NOT_SUPPORTED";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void testUsernameValidationUsingShortUsername() {
        userValidator = new RegisterUserValidator(VERY_SHORT_USERNAME, EMAIL, PASSWORD, VERIFY_PASSWORD);
        UsernameValidation usernameValidation = new UsernameValidation(userValidator);
        Exception exception = assertThrows(AuthenticationException.class,
                usernameValidation::validate);

        String expectedMessage = "USERNAME_LENGTH_NOT_SUPPORTED";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void testUsernameValidationUsingInvalidUsername() {
        userValidator = new RegisterUserValidator(INVALID_USERNAME, EMAIL, PASSWORD, VERIFY_PASSWORD);
        UsernameValidation usernameValidation = new UsernameValidation(userValidator);
        Exception exception = assertThrows(AuthenticationException.class,
                usernameValidation::validate);

        String expectedMessage = "USERNAME_CAN_ONLY_CONTAIN_LOWERCASE_AND_NUMBER" +
                "_AND_MUST_BEGIN_WITH_LETTER";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test()
    public void testUsernameValidationUsingValidUsername() {
        userValidator = new RegisterUserValidator(VALID_USERNAME, EMAIL, PASSWORD, VERIFY_PASSWORD);
        UsernameValidation usernameValidation = new UsernameValidation(userValidator);

        assertDoesNotThrow(usernameValidation::validate);
    }
}
