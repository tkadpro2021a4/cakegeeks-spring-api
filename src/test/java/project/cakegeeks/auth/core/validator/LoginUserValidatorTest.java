package project.cakegeeks.auth.core.validator;

import project.cakegeeks.auth.core.exception.AuthenticationException;
import project.cakegeeks.auth.model.User;
import project.cakegeeks.auth.repository.UserRepository;

import at.favre.lib.crypto.bcrypt.BCrypt;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

public class LoginUserValidatorTest {
    private final String USERNAME = "budi77";
    private final String EMAIL = "budi@email.com";
    private final String PASSWORD = "Budi1oke!";
    private final String WRONG_PASSWORD = "Asiaa@@p123";

    private final int PASSWORD_SALT = 7;

    @InjectMocks
    private LoginUserValidator loginUserValidator;

    private UserRepository userRepository;

    private User user;

    @BeforeEach
    public void setUp() {
        userRepository = Mockito.mock(UserRepository.class);
        LoginUserValidator.setUserRepository(userRepository);

        loginUserValidator = new LoginUserValidator(USERNAME, PASSWORD);
        user = new User();
        user.setUsername(USERNAME);
        user.setEmail(EMAIL);
        user.setPassword(hasher(PASSWORD));
    }

    private String hasher(String plaintext) {
        String ciphertext = BCrypt.withDefaults().hashToString(PASSWORD_SALT, plaintext.toCharArray());
        return ciphertext;
    }

    @Test
    public void testLoginWithNonExistentUser() {
        Optional<User> optionalUser = Optional.empty();

        when(userRepository.findByUsername(USERNAME)).thenReturn(optionalUser);

        Exception exception = assertThrows(AuthenticationException.class,
                loginUserValidator::login);

        String expectedMessage = "WRONG_USERNAME_OR_PASSWORD";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void testLoginWithNonWrongPassword() {
        loginUserValidator = new LoginUserValidator(USERNAME, WRONG_PASSWORD);

        Optional<User> optionalUser = Optional.of(user);

        when(userRepository.findByUsername(USERNAME)).thenReturn(optionalUser);

        Exception exception = assertThrows(AuthenticationException.class,
                loginUserValidator::login);

        String expectedMessage = "WRONG_USERNAME_OR_PASSWORD";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void testLoginWithValidProcedure() {
        Optional<User> optionalUser = Optional.of(user);
        when(userRepository.findByUsername(USERNAME)).thenReturn(optionalUser);

        assertDoesNotThrow(loginUserValidator::login);
    }

}
