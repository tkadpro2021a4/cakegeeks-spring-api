package project.cakegeeks.auth.core.validator;

import project.cakegeeks.auth.core.exception.AuthenticationException;
import project.cakegeeks.auth.model.User;
import project.cakegeeks.auth.repository.UserRepository;

import at.favre.lib.crypto.bcrypt.BCrypt;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class RegisterUserValidatorTest {
    private final String USERNAME = "budi77";
    private final String EMAIL = "budi@email.com";
    private final String PASSWORD = "Budi1oke!";
    private final String WRONG_PASSWORD = "Asiaa@@p123";

    private final String EXISTING_USERNAME = "budigemink";
    private final String EXISING_EMAIL = "budigemink@mantap.com";

    private final int PASSWORD_SALT = 7;

    @InjectMocks
    private RegisterUserValidator registerUserValidator;

    private UserRepository userRepository;

    private User user;

    private Optional<User> existingUser;
    private Optional<User> nonExistingUser;

    @BeforeEach
    public void setUp() {
        userRepository = Mockito.mock(UserRepository.class);
        RegisterUserValidator.setUserRepository(userRepository);

        user = new User();
        user.setUsername(EXISTING_USERNAME);
        user.setEmail(EXISING_EMAIL);
        user.setPassword(hasher(PASSWORD));

        existingUser = Optional.of(user);
        nonExistingUser = Optional.empty();
    }

    private String hasher(String plaintext) {
        String ciphertext = BCrypt.withDefaults().hashToString(PASSWORD_SALT, plaintext.toCharArray());
        return ciphertext;
    }

    @Test
    public void testRegisterWithExistentUser() {
        registerUserValidator = new RegisterUserValidator(EXISTING_USERNAME, EMAIL, PASSWORD, PASSWORD);

        when(userRepository.findByUsername(EXISTING_USERNAME)).thenReturn(existingUser);
        lenient().when(userRepository.findByEmail(EMAIL)).thenReturn(nonExistingUser);

        Exception exception = assertThrows(AuthenticationException.class,
                registerUserValidator::register);

        String expectedMessage = "USERNAME_ALREADY_EXISTS";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void testRegisterWithExistentEmail() {
        registerUserValidator = new RegisterUserValidator(USERNAME, EXISING_EMAIL, PASSWORD, PASSWORD);

        when(userRepository.findByUsername(USERNAME)).thenReturn(nonExistingUser);
        when(userRepository.findByEmail(EXISING_EMAIL)).thenReturn(existingUser);

        Exception exception = assertThrows(AuthenticationException.class,
                registerUserValidator::register);

        String expectedMessage = "EMAIL_ALREADY_EXISTS";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void testRegisterWithDifferentPassword() {
        registerUserValidator = new RegisterUserValidator(USERNAME, EMAIL, PASSWORD, WRONG_PASSWORD);

        when(userRepository.findByUsername(USERNAME)).thenReturn(nonExistingUser);
        when(userRepository.findByEmail(EMAIL)).thenReturn(nonExistingUser);

        Exception exception = assertThrows(AuthenticationException.class,
                registerUserValidator::register);

        String expectedMessage = "PASSWORD_DOES_NOT_MATCH";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void testRegisterWithValidProcedure() {
        registerUserValidator = new RegisterUserValidator(USERNAME, EMAIL, PASSWORD, PASSWORD);

        when(userRepository.findByUsername(USERNAME)).thenReturn(nonExistingUser);
        when(userRepository.findByEmail(EMAIL)).thenReturn(nonExistingUser);

        // Step 1: Check if exception not thrown
        assertDoesNotThrow(registerUserValidator::register);

        // Step 2: Check save being invoked once
        verify(userRepository, times(1)).save(any(User.class));
    }
}
