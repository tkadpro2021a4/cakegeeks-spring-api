package project.cakegeeks.auth.model;

import at.favre.lib.crypto.bcrypt.BCrypt;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UserTest {
    private final String USERNAME = "budi77";
    private final String EMAIL = "budi77@gmel.kom";
    private final String PASSWORD = "budigemiNk344123";
    private final int PASSWORD_SALT = 7;

    private User user;

    @BeforeEach
    public void setUp() {
        user = new User();
        user.setUsername(USERNAME);
        user.setEmail(EMAIL);
        user.setPassword(hasher(PASSWORD));
    }

    private String hasher(String plaintext) {
        String ciphertext = BCrypt.withDefaults().hashToString(PASSWORD_SALT, plaintext.toCharArray());
        return ciphertext;
    }

    @Test
    public void testID() {
        long actual = user.getId();
        System.out.println(actual);

        assertEquals(0L, actual);
    }

    @Test
    public void testUsername() {
        String actual = user.getUsername();

        assertEquals(USERNAME, actual);
    }

    @Test
    public void testEmail() {
        String actual = user.getEmail();

        assertEquals(EMAIL, actual);
    }

    @Test
    public void testPassword() {
        String hashedPassword = user.getPassword();

        boolean correctPassword = BCrypt.verifyer().verify(PASSWORD.toCharArray(), hashedPassword).verified;
        assertTrue(correctPassword);
    }

    @Test
    public void testEqualUser() {
        User resultUser = user;

        assertEquals(User.class, resultUser.getClass());
        assertEquals(user, resultUser);
    }

}
