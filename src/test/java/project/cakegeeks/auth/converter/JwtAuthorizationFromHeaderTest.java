package project.cakegeeks.auth.converter;

import project.cakegeeks.auth.core.exception.AuthorizationException;
import project.cakegeeks.auth.util.Jwt;
import project.cakegeeks.auth.util.JwtAuthorization;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.web.server.ResponseStatusException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class JwtAuthorizationFromHeaderTest {

    private JwtAuthorization jwtAuthorization;

    @Mock
    Jwt jwt = new Jwt("dummykeyhellobandunks eheehhehehe xdskyntbskck17032001");

    @InjectMocks
    private JwtAuthorizationFromHeader jwtAuthorizationFromHeader;

    @Test
    public void testConvertFailed() throws Exception {
        jwtAuthorization = Mockito.mock(JwtAuthorization.class);
        jwtAuthorizationFromHeader = new JwtAuthorizationFromHeader();
        doThrow(new AuthorizationException("BAD_REQUEST_TOKEN")).when(jwtAuthorization).authorize(any(String.class));

        String token = "token";

        Exception exception = assertThrows(ResponseStatusException.class,() -> {
            jwtAuthorizationFromHeader.convert(token);
        });

        String expectedMessage = "BAD_REQUEST_TOKEN";

        // Check inner exception
        Exception cause = (Exception) exception.getCause();
        assertEquals(AuthorizationException.class, cause.getClass());
        assertEquals(expectedMessage, cause.getMessage());

    }

    @Test
    public void testConvertSuccess() {
        jwt = Mockito.mock(Jwt.class);
        jwtAuthorizationFromHeader = new JwtAuthorizationFromHeader(jwt);

        when(jwt.validateToken(any(String.class))).thenReturn(true);
        String token = "Bearer token";

        JwtAuthorization result = jwtAuthorizationFromHeader.convert(token);
        String tokenResult = result.getJwtToken();

        assertEquals("token", tokenResult);
    }
}
