package project.cakegeeks.auth.service;

import project.cakegeeks.auth.core.exception.AuthenticationException;
import project.cakegeeks.auth.core.validator.LoginUserValidator;
import project.cakegeeks.auth.core.validator.RegisterUserValidator;
import project.cakegeeks.auth.model.User;
import project.cakegeeks.auth.repository.UserRepository;
import project.cakegeeks.auth.util.Jwt;

import at.favre.lib.crypto.bcrypt.BCrypt;
import java.util.Map;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

public class UserServiceImplTest {
    private final String USERNAME = "budi77";
    private final String EMAIL = "budi@email.com";
    private final String PASSWORD = "Budi1oke!";
    private final String WRONG_PASSWORD = "Asiaa@@p123";

    private final String EXISTING_USERNAME = "budigemink";
    private final String EXISING_EMAIL = "budigemink@mantap.com";

    private final int PASSWORD_SALT = 7;
    private final String JWT_TOKEN = "Jwteheskuyyy";

    @InjectMocks
    private RegisterUserValidator registerUserValidator;

    @InjectMocks
    private LoginUserValidator loginUserValidator;

    @Mock
    private UserRepository userRepository;

    @Mock
    private Jwt jwt;

    @InjectMocks
    private UserServiceImpl userService = new UserServiceImpl();

    private User user;
    private Optional<User> existingUser;
    private Optional<User> nonExistingUser;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        RegisterUserValidator.setUserRepository(userRepository);
        LoginUserValidator.setUserRepository(userRepository);

        user = new User();
        user.setUsername(EXISTING_USERNAME);
        user.setEmail(EXISING_EMAIL);
        user.setPassword(hasher(PASSWORD));

        existingUser = Optional.of(user);
        nonExistingUser = Optional.empty();
    }

    private String hasher(String plaintext) {
        String ciphertext = BCrypt.withDefaults().hashToString(PASSWORD_SALT, plaintext.toCharArray());
        return ciphertext;
    }

    @Test
    public void testServiceLoginFailed() {
        loginUserValidator = new LoginUserValidator(USERNAME, PASSWORD);

        when(userRepository.findByUsername(USERNAME)).thenReturn(nonExistingUser);

        Exception exception = assertThrows(AuthenticationException.class,() -> {
            userService.login(loginUserValidator);
        });

        String expectedMessage = "WRONG_USERNAME_OR_PASSWORD";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));

    }

    @Test
    public void testServiceLoginSuccess() {
        loginUserValidator = new LoginUserValidator(EXISTING_USERNAME, PASSWORD);

        when(userRepository.findByUsername(EXISTING_USERNAME)).thenReturn(existingUser);
        when(jwt.createToken(EXISTING_USERNAME)).thenReturn(JWT_TOKEN);

        try {
            Map<String, Object> response = userService.login(loginUserValidator);

            // Step 1: Check size
            assertEquals(2, response.size());

            // Step 2: Check all field
            assertTrue(response.containsKey("Authorization"));
            assertTrue(response.containsKey("username"));

            // Step 3: Check value
            assertEquals(String.class, response.get("Authorization").getClass());
            assertEquals(JWT_TOKEN, response.get("Authorization"));
            assertEquals(EXISTING_USERNAME, response.get("username"));

        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testServiceRegisterFailed() {
        registerUserValidator = new RegisterUserValidator(EXISTING_USERNAME, EXISING_EMAIL, PASSWORD, PASSWORD);

        when(userRepository.findByUsername(EXISTING_USERNAME)).thenReturn(existingUser);

        Exception exception = assertThrows(AuthenticationException.class,() -> {
            userService.register(registerUserValidator);
        });

        String expectedMessage = "USERNAME_ALREADY_EXISTS";
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void testServiceRegisterSuccess() {
        registerUserValidator = new RegisterUserValidator(USERNAME, EMAIL, PASSWORD, PASSWORD);

        User newUser = new User();
        newUser.setUsername(USERNAME);
        newUser.setEmail(EMAIL);
        newUser.setPassword(hasher(PASSWORD));

        Optional<User> optNewUser = Optional.of(newUser);

        when(userRepository.findByUsername(EXISTING_USERNAME)).thenReturn(existingUser);
        when(userRepository.findByUsername(USERNAME)).thenReturn(nonExistingUser).thenReturn(optNewUser);

        try {
            Map<String, Object> response = userService.register(registerUserValidator);

            // Step 1: Check response size
            assertEquals(3, response.size());

            // Step 2: Check all field
            assertTrue(response.containsKey("username"));
            assertTrue(response.containsKey("email"));
            assertTrue(response.containsKey("hashedPassword"));

            // Step 3: Check all value
            assertEquals(USERNAME, response.get("username"));
            assertEquals(EMAIL, response.get("email"));
            String hashedPassword = response.get("hashedPassword").toString();
            boolean correctPassword = BCrypt.verifyer().verify(PASSWORD.toCharArray(), hashedPassword).verified;
            assertTrue(correctPassword);

        } catch (Exception e) {
            System.out.println(e.getMessage());
            fail();
        }
    }
}
