package project.cakegeeks;

import org.junit.jupiter.api.Test;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import project.cakegeeks.auth.properties.AuthProperty;

@SpringBootTest
@EnableConfigurationProperties(AuthProperty.class)
class CakeGeeksApplicationTests {

	@Test
	void contextLoads() {
	}

}
