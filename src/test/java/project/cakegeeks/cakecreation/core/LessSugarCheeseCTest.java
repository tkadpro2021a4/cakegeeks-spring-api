package project.cakegeeks.cakecreation.core;

import java.lang.reflect.Method;
import javassist.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class LessSugarCheeseCTest {
    private Class<?> lsCheeseClass;

    @BeforeEach
    public void setUp() throws Exception {
        lsCheeseClass = Class.forName("project.cakegeeks.cakecreation.core.LessSugarCheeseC");
    }

    @Test
    public void testLessSugarCheeseCIsConcreteClass() throws Exception {
        assertFalse(Modifier.isAbstract(lsCheeseClass.getModifiers()));
    }

    @Test
    public void testLessSugarCheeseCOverrideGetNameMethod() throws Exception {
        Method getName = lsCheeseClass.getDeclaredMethod("getName");
        assertTrue(Modifier.isPublic(getName.getModifiers()));
        assertEquals(0, getName.getParameterCount());
    }

    @Test
    public void getNameTest() throws Exception {
        LessSugarCheeseC ls = new LessSugarCheeseC("Less Sugar Cheese Cake", 75000);
        assertEquals("Less Sugar Cheese Cake", ls.getName());
    }

    @Test
    public void testLessSugarCheeseCOverrideGetPriceMethod() throws Exception {
        Method getPrice = lsCheeseClass.getDeclaredMethod("getPrice");
        assertTrue(Modifier.isPublic(getPrice.getModifiers()));
        assertEquals(0, getPrice.getParameterCount());
    }

    @Test
    public void getPriceTest() {
        LessSugarCheeseC ls = new LessSugarCheeseC("Less Sugar Cheese Cake", 75000);
        assertEquals(73000, ls.getPrice());
    }

    @Test
    public void testLessSugarCheeseCOverrideGetTypeMethod() throws Exception {
        Method getType = lsCheeseClass.getDeclaredMethod("getType");
        assertTrue(Modifier.isPublic(getType.getModifiers()));
        assertEquals(0, getType.getParameterCount());
    }

    @Test
    public void getTypeTest() {
        LessSugarCheeseC ls = new LessSugarCheeseC("Less Sugar Cheese Cake", 75000);
        assertEquals("Less Sugar Cheese Cake", ls.getType());
    }
}
