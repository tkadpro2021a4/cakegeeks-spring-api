package project.cakegeeks.cakecreation.core;

import java.lang.reflect.Method;
import javassist.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OgChiffonTest {
    private Class<?> ogChiffonC;

    @BeforeEach
    public void setUp() throws Exception {
        ogChiffonC = Class.forName("project.cakegeeks.cakecreation.core.OgChiffonC");
    }

    @Test
    public void testOgChiffonCIsConcreteClass() throws Exception {
        assertFalse(Modifier.isAbstract(ogChiffonC.getModifiers()));
    }

    @Test
    public void testOgChiffonCOverrideGetNameMethod() throws Exception {
        Method getName = ogChiffonC.getDeclaredMethod("getName");
        assertTrue(Modifier.isPublic(getName.getModifiers()));
        assertEquals(0, getName.getParameterCount());
    }

    @Test
    public void getNameTest() {
        OgChiffonC og = new OgChiffonC("Chiffon Cake", 70000);
        assertEquals("Chiffon Cake", og.getName());
    }

    @Test
    public void testOgChiffonCOverrideGetPriceMethod() throws Exception {
        Method getPrice = ogChiffonC.getDeclaredMethod("getPrice");
        assertTrue(Modifier.isPublic(getPrice.getModifiers()));
        assertEquals(0, getPrice.getParameterCount());
    }

    @Test
    public void getPriceTest() {
        OgChiffonC og = new OgChiffonC("Chiffon Cake", 70000);
        assertEquals(70000, og.getPrice());
    }

    @Test
    public void testOgChiffonCOverrideGetTypeMethod() throws Exception {
        Method getType = ogChiffonC.getDeclaredMethod("getType");
        assertTrue(Modifier.isPublic(getType.getModifiers()));
        assertEquals(0, getType.getParameterCount());
    }

    @Test
    public void getTypeTest() {
        OgChiffonC og = new OgChiffonC("Chiffon Cake", 70000);
        assertEquals("Chiffon Cake", og.getType());
    }
}
