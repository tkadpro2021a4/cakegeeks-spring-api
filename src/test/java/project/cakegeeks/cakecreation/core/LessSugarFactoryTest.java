package project.cakegeeks.cakecreation.core;

import java.lang.reflect.Method;
import javassist.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LessSugarFactoryTest {
    private Class<?> lessSugar;

    @BeforeEach
    public void setUp() throws Exception {
        lessSugar = Class.forName("project.cakegeeks.cakecreation.core.LessSugarFactory");
    }

    @Test
    public void testLessSugarFactoryCIsConcreteClass() throws Exception {
        assertFalse(Modifier.isAbstract(lessSugar.getModifiers()));
    }

    @Test
    public void testLessSugarFactoryOverrideCreateCheeseCMethod() throws Exception {
        Method createCheeseC = lessSugar.getDeclaredMethod("createCheeseC", String.class, int.class);
        assertTrue(Modifier.isPublic(createCheeseC.getModifiers()));
        assertEquals(2, createCheeseC.getParameterCount());
    }

    @Test
    public void createCheeseCTest() {
        LessSugarFactory ls = new LessSugarFactory();
        CheeseCake cheeseCake = ls.createCheeseC("Cheese Cake", 75000);
        LessSugarCheeseC lessSugarCheeseC = new LessSugarCheeseC("Cheese Cake", 75000);
        assertEquals(cheeseCake.getName(), lessSugarCheeseC.getName());
        assertEquals(cheeseCake.getPrice(), lessSugarCheeseC.getPrice());
    }

    @Test
    public void testLessSugarFactoryOverrideCreateChiffonCMethod() throws Exception {
        Method createChiffonC = lessSugar.getDeclaredMethod("createChiffonC", String.class, int.class);
        assertTrue(Modifier.isPublic(createChiffonC.getModifiers()));
        assertEquals(2, createChiffonC.getParameterCount());
    }

    @Test
    public void createChiffonCTest() {
        LessSugarFactory ls = new LessSugarFactory();
        ChiffonCake chiffonCake = ls.createChiffonC("Chiffon Cake", 70000);
        LessSugarChiffonC lessSugarChiffonC = new LessSugarChiffonC("Chiffon Cake", 70000);
        assertEquals(chiffonCake.getName(), lessSugarChiffonC.getName());
        assertEquals(chiffonCake.getPrice(), lessSugarChiffonC.getPrice());
    }
}