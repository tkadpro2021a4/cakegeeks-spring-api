package project.cakegeeks.cakecreation.core;

import java.lang.reflect.Method;
import javassist.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OgFactoryTest {
    private Class<?> og;

    @BeforeEach
    public void setUp() throws Exception {
        og = Class.forName("project.cakegeeks.cakecreation.core.OgFactory");
    }

    @Test
    public void testOgFactoryIsConcreteClass() throws Exception {
        assertFalse(Modifier.isAbstract(og.getModifiers()));
    }

    @Test
    public void testOgFactoryOverrideCreateCheeseCMethod() throws Exception {
        Method createCheeseC = og.getDeclaredMethod("createCheeseC", String.class, int.class);
        assertTrue(Modifier.isPublic(createCheeseC.getModifiers()));
        assertEquals(2, createCheeseC.getParameterCount());
    }

    @Test
    public void createCheeseCTest() {
        OgFactory ogF = new OgFactory();
        CheeseCake cheeseCake = ogF.createCheeseC("Cheese Cake", 75000);
        OgCheeseC ogCheeseC = new OgCheeseC("Cheese Cake", 75000);
        assertEquals(cheeseCake.getName(), ogCheeseC.getName());
        assertEquals(cheeseCake.getPrice(), ogCheeseC.getPrice());
    }

    @Test
    public void testOgFactoryOverrideCreateChiffonCMethod() throws Exception {
        Method createChiffonC = og.getDeclaredMethod("createChiffonC", String.class, int.class);
        assertTrue(Modifier.isPublic(createChiffonC.getModifiers()));
        assertEquals(2, createChiffonC.getParameterCount());
    }

    @Test
    public void createChiffonCTest() {
        OgFactory ogF = new OgFactory();
        ChiffonCake chiffonCake = ogF.createChiffonC("Chiffon Cake", 70000);
        OgChiffonC ogChiffonC = new OgChiffonC("Chiffon Cake", 70000);
        assertEquals(chiffonCake.getName(), ogChiffonC.getName());
        assertEquals(chiffonCake.getPrice(), ogChiffonC.getPrice());
    }
}
