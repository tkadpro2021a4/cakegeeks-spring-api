package project.cakegeeks.cakecreation.core;

import java.lang.reflect.Method;
import javassist.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OgCheeseCTest {
    private Class<?> ogCheeseC;

    @BeforeEach
    public void setUp() throws Exception {
        ogCheeseC = Class.forName("project.cakegeeks.cakecreation.core.OgCheeseC");
    }

    @Test
    public void testOgCheeseCIsConcreteClass() throws Exception {
        assertFalse(Modifier.isAbstract(ogCheeseC.getModifiers()));
    }

    @Test
    public void testOgCheeseCOverrideGetNameMethod() throws Exception {
        Method getName = ogCheeseC.getDeclaredMethod("getName");
        assertTrue(Modifier.isPublic(getName.getModifiers()));
        assertEquals(0, getName.getParameterCount());
    }

    @Test
    public void getNameTest() {
        OgCheeseC og = new OgCheeseC("Cheese Cake", 75000);
        assertEquals("Cheese Cake", og.getName());
    }

    @Test
    public void testOgCheeseCOverrideGetPriceMethod() throws Exception {
        Method getPrice = ogCheeseC.getDeclaredMethod("getPrice");
        assertTrue(Modifier.isPublic(getPrice.getModifiers()));
        assertEquals(0, getPrice.getParameterCount());
    }

    @Test
    public void getPriceTest() {
        OgCheeseC og = new OgCheeseC("Cheese Cake", 75000);
        assertEquals(75000, og.getPrice());
    }

    @Test
    public void testOgCheeseCOverrideGetTypeMethod() throws Exception {
        Method getType = ogCheeseC.getDeclaredMethod("getType");
        assertTrue(Modifier.isPublic(getType.getModifiers()));
        assertEquals(0, getType.getParameterCount());
    }

    @Test
    public void getTypeTest() {
        OgCheeseC og = new OgCheeseC("Cheese Cake", 75000);
        assertEquals("Cheese Cake", og.getType());
    }
}