package project.cakegeeks.cakecreation.core;

import java.lang.reflect.Method;
import javassist.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LessSugarChiffonCTest {
    private Class<?> lsChiffonClass;

    @BeforeEach
    public void setUp() throws Exception {
        lsChiffonClass = Class.forName("project.cakegeeks.cakecreation.core.LessSugarChiffonC");
    }

    @Test
    public void testLessSugarChiffonCIsConcreteClass() throws Exception {
        assertFalse(Modifier.isAbstract(lsChiffonClass.getModifiers()));
    }

    @Test
    public void testLessSugarChiffonCOverrideGetNameMethod() throws Exception {
        Method getName = lsChiffonClass.getDeclaredMethod("getName");
        assertTrue(Modifier.isPublic(getName.getModifiers()));
        assertEquals(0, getName.getParameterCount());
    }

    @Test
    public void getNameTest() throws Exception {
        LessSugarChiffonC ls = new LessSugarChiffonC("Less Sugar Chiffon Cake", 70000);
        assertEquals("Less Sugar Chiffon Cake", ls.getName());
    }

    @Test
    public void testLessSugarChiffonCOverrideGetPriceMethod() throws Exception {
        Method getPrice = lsChiffonClass.getDeclaredMethod("getPrice");
        assertTrue(Modifier.isPublic(getPrice.getModifiers()));
        assertEquals(0, getPrice.getParameterCount());
    }

    @Test
    public void getPriceTest() {
        LessSugarChiffonC ls = new LessSugarChiffonC("Less Sugar Chiffon Cake", 70000);
        assertEquals(68000, ls.getPrice());
    }

    @Test
    public void testLessSugarCheeseCOverrideGetTypeMethod() throws Exception {
        Method getType = lsChiffonClass.getDeclaredMethod("getType");
        assertTrue(Modifier.isPublic(getType.getModifiers()));
        assertEquals(0, getType.getParameterCount());
    }

    @Test
    public void getTypeTest() {
        LessSugarChiffonC ls = new LessSugarChiffonC("Less Sugar Chiffon Cake", 70000);
        assertEquals("Less Sugar Chiffon Cake", ls.getType());
    }
}
