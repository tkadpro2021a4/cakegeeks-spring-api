package project.cakegeeks.cakecreation.controller;

import project.cakegeeks.cakecreation.core.Cake;
import project.cakegeeks.cakecreation.core.OgCheeseC;
import project.cakegeeks.cakecreation.model.CakeModel;
import project.cakegeeks.cakecreation.service.CakeService;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import project.cakegeeks.auth.util.Jwt;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = CakeCreationController.class)
public class CakeCreationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    Jwt jwt;

    @MockBean
    private CakeService cakeService;

    @Test
    public void testAddCakeURL() throws Exception {

        mockMvc.perform(get("/addcake"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("chooseCake"))
                .andExpect(view().name("creation/addcake"));
    }

    @Test
    public void testNextStep() throws Exception {
        Map<String, Object> request = new HashMap<>();
        request.put("cake", "cheese");
        request.put("custom", "original");
        final String json = new ObjectMapper().writeValueAsString(request);
        final Cake cake = new OgCheeseC("cheese", 20000);
        CakeModel cakeModel = new CakeModel();
        cakeModel.setName("cheese");
        cakeModel.setType("cheese");
        cakeModel.setPrice(20000);

        when(cakeService.createCake("cheese", "original")).thenReturn(cake);
        when(cakeService.addCake(cake)).thenReturn(cakeModel);

        mockMvc.perform(post("/nextstep")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("nextStep"))
                .andExpect(jsonPath("$.name").value("cheese"))
                .andExpect(jsonPath("$.type").value("cheese"))
                .andExpect(jsonPath("$.price").value(20000));
    }
}
