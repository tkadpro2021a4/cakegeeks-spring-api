package project.cakegeeks.cakecreation.model;

import java.lang.reflect.Method;
import javassist.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CakeModelTest {
    private Class<?> cake;

    @BeforeEach
    public void setUp() throws Exception {
        cake = Class.forName("project.cakegeeks.cakecreation.model.CakeModel");
    }

    @Test
    public void testCakeModelIsConcreteClass() throws Exception {
        assertFalse(Modifier.isAbstract(cake.getModifiers()));
    }

    @Test
    public void testCakeModelOverrideSetIdMethod() throws Exception {
        Method setId = cake.getDeclaredMethod("setId", long.class);
        assertTrue(Modifier.isPublic(setId.getModifiers()));
        assertEquals(1, setId.getParameterCount());
    }

    @Test
    public void testCakeModelOverrideGetIdMethod() throws Exception {
        Method getId = cake.getDeclaredMethod("getId");
        assertTrue(Modifier.isPublic(getId.getModifiers()));
        assertEquals(0, getId.getParameterCount());
    }

    @Test
    public void setIdTest() {
        CakeModel cakeModel = new CakeModel("Cheese Cake", "Cheese Cake", 75000);
        cakeModel.setId(1);
        assertEquals(1, cakeModel.getId());
    }

    @Test
    public void testCakeModelOverrideGetNameMethod() throws Exception {
        Method getName = cake.getDeclaredMethod("getName");
        assertTrue(Modifier.isPublic(getName.getModifiers()));
        assertEquals(0, getName.getParameterCount());
    }

    @Test
    public void getNameTest() {
        CakeModel cakeModel = new CakeModel("Cheese Cake", "Cheese Cake", 75000);
        assertEquals("Cheese Cake", cakeModel.getName());
    }

    @Test
    public void testCakeModelOverrideGetTypeMethod() throws Exception {
        Method getType = cake.getDeclaredMethod("getType");
        assertTrue(Modifier.isPublic(getType.getModifiers()));
        assertEquals(0, getType.getParameterCount());
    }

    @Test
    public void getTypeTest() {
        CakeModel cakeModel = new CakeModel("Cheese Cake", "Cheese Cake", 75000);
        assertEquals("Cheese Cake", cakeModel.getType());
    }

    @Test
    public void testCakeModelOverrideGetPriceMethod() throws Exception {
        Method getPrice = cake.getDeclaredMethod("getPrice");
        assertTrue(Modifier.isPublic(getPrice.getModifiers()));
        assertEquals(0, getPrice.getParameterCount());
    }

    @Test
    public void getPriceTest() {
        CakeModel cakeModel = new CakeModel("Cheese Cake", "Cheese Cake", 75000);
        assertEquals(75000, cakeModel.getPrice());
    }
}
