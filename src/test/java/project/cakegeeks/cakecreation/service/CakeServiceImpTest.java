package project.cakegeeks.cakecreation.service;

import project.cakegeeks.cakecreation.core.Cake;
import project.cakegeeks.cakecreation.core.OgCheeseC;
import project.cakegeeks.cakecreation.model.CakeModel;
import project.cakegeeks.cakecreation.repository.CakeRepository;

import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CakeServiceImpTest {
    @Mock
    private CakeRepository cakeRepository;
    private CakeModel cakeModel;
    private Cake cake;

    @InjectMocks
    private CakeServiceImp cakeService;

    @BeforeEach
    public void setUp() {
        cakeModel = new CakeModel(
                "Cheese Cake",
                "Cheese Cake",
                75000
        );
        cakeModel.setId(1);
        cake = new OgCheeseC("Cheese Cake", 75000);
    }

    @Test
    public void findAllTest() {
        List<CakeModel> cakeList = cakeService.findAll();
        lenient().when(cakeService.findAll()).thenReturn(cakeList);
    }

    @Test
    public void findCakeTest() {
        cakeService.register(cakeModel);
        Optional<CakeModel> cakeModelExist = cakeService.findCake(cakeModel.getId());
        Optional<CakeModel> cakeModelDoesntExist = cakeService.findCake((long) 2);
        lenient().when(cakeService.findCake(cakeModel.getId())).thenReturn(Optional.of(cakeModel));
    }

    @Test
    public void eraseTest() {
        cakeService.register(cakeModel);
        cakeService.erase(cakeModel.getId());
        lenient().when(cakeService.findCake(cakeModel.getId())).thenReturn(Optional.empty());
    }

    @Test
    public void registerTest() {
        cakeService.register(cakeModel);
        lenient().when(cakeService.register(cakeModel)).thenReturn(cakeModel);
    }

    @Test
    public void createCakeTest() {
        String base = "Cheese Cake";
        String baseChoice = "OG";
        String base2 = "Chiffon Cake";
        String baseChoice2 = "Less";
        assertEquals(cakeService.createCake(base, baseChoice) instanceof Cake, true);
        assertEquals(cakeService.createCake(base, baseChoice2) instanceof Cake, true);
        assertEquals(cakeService.createCake(base2, baseChoice) instanceof Cake, true);
        assertEquals(cakeService.createCake(base2, baseChoice2) instanceof Cake, true);
    }

    @Test
    public void addCakeTest() {
        cakeModel = cakeService.addCake(cake);
        lenient().when(cakeService.addCake(cake)).thenReturn(cakeModel);
    }

    @Test
    public void rewriteCakeTest() {
        CakeModel cakeModel = new CakeModel();
        cakeModel.setName("cheese");
        cakeModel.setType("cheese");
        cakeModel.setPrice(20000);
        Optional<CakeModel> optionalCakeModel = Optional.of(cakeModel);

        when(cakeRepository.findById(any(Long.class))).thenReturn(optionalCakeModel);

        CakeModel result = cakeService.rewrite(cakeModel);

        assertEquals(cakeModel, result);
        verify(cakeRepository, times(1)).save(any());
    }
}
