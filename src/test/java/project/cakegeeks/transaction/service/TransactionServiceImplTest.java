package project.cakegeeks.transaction.service;

import project.cakegeeks.transaction.model.TransactionModel;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import project.cakegeeks.cakecreation.model.CakeModel;
import project.cakegeeks.cakecreation.service.CakeService;
import project.cakegeeks.cakecreation.service.CakeServiceImp;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TransactionServiceImplTest {

    @InjectMocks
    TransactionService transactionService = new TransactionServiceImpl();

    @InjectMocks
    CakeService cakeService = new CakeServiceImp();

    @Test
    public void testMethodMakeTransaction() throws NoSuchMethodException {
        Method getName = transactionService.getClass().getDeclaredMethod("makeTransaction",CakeModel.class);
        assertTrue(Modifier.isPublic(getName.getModifiers()));
        assertEquals(1, getName.getParameterCount());
        TransactionModel transactionModel = transactionService.makeTransaction(new CakeModel("Cheese Cake", "Cheese Cake", 75000));
    }
}
