package project.cakegeeks.transaction.controller;

import project.cakegeeks.transaction.model.TransactionModel;
import project.cakegeeks.transaction.service.TransactionService;

import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import project.cakegeeks.auth.util.Jwt;
import project.cakegeeks.cakecreation.model.CakeModel;
import project.cakegeeks.cakecreation.service.CakeService;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = TransactionController.class)
public class TransactionControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    Jwt jwt;

    @MockBean
    private CakeService cakeService;

    @MockBean
    private TransactionService transactionService;

    @Test
    public void testGetTransaction() throws Exception {
        CakeModel cakeModel = new CakeModel();
        cakeModel.setName("cheese");
        cakeModel.setType("cheese");
        cakeModel.setPrice(20000);
        Optional<CakeModel> optionalCakeModel = Optional.of(cakeModel);

        TransactionModel transactionModel = new TransactionModel(cakeModel);

        when(cakeService.findCake(1L)).thenReturn(optionalCakeModel);
        when(transactionService.makeTransaction(cakeModel)).thenReturn(transactionModel);

        mockMvc.perform(get("/transaction/1/order"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("getTransaction"))
                .andExpect(model().attributeExists("cake_name"))
                .andExpect(model().attributeExists("cake_price"))
                .andExpect(model().attributeExists("state"))
                .andExpect(model().attribute("cake_name", "cheese"))
                .andExpect(model().attribute("cake_price", 20000))
                .andExpect(model().attribute("state", "Start State"))
                .andExpect(view().name("transaction/transaction"));

        mockMvc.perform(post("/transaction/ordersuccess"))
                .andExpect(status().is3xxRedirection())
                .andExpect(handler().methodName("processingTransaction"))
                .andExpect(view().name("redirect:/transaction/0/order"));
    }
}
