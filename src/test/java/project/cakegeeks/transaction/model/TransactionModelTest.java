package project.cakegeeks.transaction.model;

import project.cakegeeks.transaction.core.StartState;

import javassist.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import project.cakegeeks.cakecreation.model.CakeModel;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class TransactionModelTest {
    private Class<?> TransactionModelClass;

    @BeforeEach
    public void setUp() throws Exception {
        TransactionModelClass = Class.forName("project.cakegeeks.transaction.model.TransactionModel");
    }

    @Test
    public void testTrasactionModelClassisConcreteClass() throws Exception {
        assertFalse(Modifier.isAbstract(TransactionModelClass.getModifiers()));
    }

    @Test
    public void testTransactionModelsetandgetTransactionState() throws Exception {
        TransactionModel transactionModel = new TransactionModel(new CakeModel("Cheese Cake", "Cheese Cake", 75000));
        transactionModel.setTransactionState(new StartState());
        assertEquals("Start State",transactionModel.getTransactionState().tostring());
    }

    @Test
    public void testTransactionModelnextState() throws Exception {
        TransactionModel transactionModel = new TransactionModel(new CakeModel("Cheese Cake", "Cheese Cake", 75000));
        transactionModel.nextState();
        assertEquals("Pesanan Telah Siap",transactionModel.getTransactionState().tostring());
    }

    @Test
    public void testTransactionModelgetCakeName() throws Exception {
        TransactionModel transactionModel = new TransactionModel(new CakeModel("Cheese Cake", "Cheese Cake", 75000));
        assertEquals("Cheese Cake",transactionModel.getCakeName());
    }

    @Test
    public void testTransactionModelgetCakePrice() throws Exception {
        TransactionModel transactionModel = new TransactionModel(new CakeModel("Cheese Cake", "Cheese Cake", 75000));
        assertEquals(75000,transactionModel.getCakePrice());
    }

    @Test
    public void testTransactionModelgetCakeType() throws Exception {
        TransactionModel transactionModel = new TransactionModel(new CakeModel("Cheese Cake", "Cheese Cake", 75000));
        assertEquals("Cheese Cake",transactionModel.getCakeType());
    }
}
