package project.cakegeeks.transaction.core;

import java.lang.reflect.Method;
import javassist.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StartStateTest {
    private Class<?> StartStateclass;

    @BeforeEach
    public void setUp() throws Exception {
        StartStateclass = Class.forName("project.cakegeeks.transaction.core.StartState");
    }

    @Test
    public void testStartStateClassisConcreteClass() throws Exception {
        assertFalse(Modifier.isAbstract(StartStateclass.getModifiers()));
    }

    @Test
    public void testStartStateClassOverrideToStringMethod() throws Exception {
        Method getName = StartStateclass.getDeclaredMethod("tostring");
        assertTrue(Modifier.isPublic(getName.getModifiers()));
        assertEquals(0, getName.getParameterCount());
    }

    @Test
    public void tostringTest() {
        StartState startStates = new StartState();
        assertEquals("Start State", startStates.tostring());
    }
}
