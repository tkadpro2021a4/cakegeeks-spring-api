package project.cakegeeks.transaction.core;

import java.lang.reflect.Method;
import javassist.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class EndStateTest {
    private Class<?> endStateclass;

    @BeforeEach
    public void setUp() throws Exception {
        endStateclass = Class.forName("project.cakegeeks.transaction.core.EndState");
    }

    @Test
    public void testStartStateClassisConcreteClass() throws Exception {
        assertFalse(Modifier.isAbstract(endStateclass.getModifiers()));
    }

    @Test
    public void testStartStateClassOverrideToStringMethod() throws Exception {
        Method getName = endStateclass.getDeclaredMethod("tostring");
        assertTrue(Modifier.isPublic(getName.getModifiers()));
        assertEquals(0, getName.getParameterCount());
    }

    @Test
    public void tostringTest() {
        EndState endState = new EndState();
        assertEquals("Pesanan Telah Siap", endState.tostring());
    }
}
