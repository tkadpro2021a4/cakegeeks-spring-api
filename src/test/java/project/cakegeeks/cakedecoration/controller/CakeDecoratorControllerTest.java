package project.cakegeeks.cakedecoration.controller;

import project.cakegeeks.cakedecoration.service.CakeDecoratorService;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import project.cakegeeks.auth.util.Jwt;
import project.cakegeeks.cakecreation.model.CakeModel;
import project.cakegeeks.cakecreation.service.CakeService;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = CakeDecoratorController.class)
public class CakeDecoratorControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    Jwt jwt;

    @MockBean
    private CakeService cakeService;

    @MockBean
    private CakeDecoratorService cakeDecoratorService;

    @Test
    public void testGetCakeURL() throws Exception {
        CakeModel cakeModel = new CakeModel();
        cakeModel.setName("cheese");
        cakeModel.setType("cheese");
        cakeModel.setPrice(20000);
        Optional<CakeModel> optionalCakeModel = Optional.of(cakeModel);

        when(cakeService.findCake(1L)).thenReturn(optionalCakeModel);

        mockMvc.perform(get("/1/addTopping"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("getCake"))
                .andExpect(model().attributeExists("cake"))
                .andExpect(model().attribute("cake", cakeModel))
                .andExpect(view().name("addTopping"));
    }

    @Test
    public void testAddTopping() throws Exception {
        Map<String, Object> request = new HashMap<>();
        request.put("toppings", "candy");
        final String json = new ObjectMapper().writeValueAsString(request);

        CakeModel cakeModel = new CakeModel();
        cakeModel.setName("cheese");
        cakeModel.setType("cheese");
        cakeModel.setPrice(20000);

        Optional<CakeModel> optionalCakeModel = Optional.of(cakeModel);

        when(cakeService.findCake(1L)).thenReturn(optionalCakeModel);
        when(cakeDecoratorService.addDecoration(eq(cakeModel), anyList())).thenReturn(cakeModel);
        when(cakeService.rewrite(cakeModel)).thenReturn(cakeModel);

        mockMvc.perform(post("/1/addTopping")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("addTopping"))
                .andExpect(jsonPath("$.name").value("cheese"))
                .andExpect(jsonPath("$.type").value("cheese"))
                .andExpect(jsonPath("$.price").value(20000));

    }
}
