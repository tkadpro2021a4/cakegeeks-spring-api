package project.cakegeeks.cakedecoration.service;

import project.cakegeeks.cakedecoration.core.AlmondDecorator;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import project.cakegeeks.cakecreation.core.Cake;
import project.cakegeeks.cakecreation.model.CakeModel;
import project.cakegeeks.cakecreation.service.CakeService;
import project.cakegeeks.cakecreation.service.CakeServiceImp;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CakeDecoratorServiceTest {

    private AlmondDecorator almondDecorator;
    private Cake lessSugarCheeseC;
    private List<String> decorations;
    private CakeModel cake;

    @InjectMocks
    CakeDecoratorService cakeDecoratorService = new CakeDecoratorServiceImpl();

    @InjectMocks
    CakeService cakeService = new CakeServiceImp();

    @BeforeEach
    public void setUp() {
        lessSugarCheeseC =  cakeService.createCake("Cheese", "Less");
        cake = new CakeModel(lessSugarCheeseC.getName(), lessSugarCheeseC.getType(), lessSugarCheeseC.getPrice());

        decorations = new ArrayList<>();
        decorations.add("Almond");
        decorations.add("Fruits");
    }

    @Test
    public void testMethodAddDecoration() throws NoSuchMethodException {
        Method getName = cakeDecoratorService.getClass().getDeclaredMethod("addDecoration", CakeModel.class, List.class);

        assertTrue(Modifier.isPublic(getName.getModifiers()));
        assertEquals(2, getName.getParameterCount());
        cake = cakeDecoratorService.addDecoration(cake, decorations);
    }

}
