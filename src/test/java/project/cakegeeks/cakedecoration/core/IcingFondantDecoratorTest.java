package project.cakegeeks.cakedecoration.core;

import project.cakegeeks.cakedecoration.service.CakeDecoratorService;
import project.cakegeeks.cakedecoration.service.CakeDecoratorServiceImpl;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import project.cakegeeks.cakecreation.core.Cake;
import project.cakegeeks.cakecreation.core.LessSugarCheeseC;
import project.cakegeeks.cakecreation.model.CakeModel;
import project.cakegeeks.cakecreation.service.CakeService;
import project.cakegeeks.cakecreation.service.CakeServiceImp;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class IcingFondantDecoratorTest {

    private Class<?> icingFondantDecoratorClass;
    private Cake lessSugarCheeseC;
    private CakeModel cake;

    @InjectMocks
    CakeService cakeService = new CakeServiceImp();

    @InjectMocks
    CakeDecoratorService cakeDecoratorService = new CakeDecoratorServiceImpl();

    @BeforeEach
    public void setUp() throws Exception {
        icingFondantDecoratorClass = Class.forName("project.cakegeeks.cakedecoration.core.IcingFondantDecorator");

        lessSugarCheeseC = new LessSugarCheeseC("Less Sugar Cheese Cake", 100000);
        List<String> decorations = new ArrayList<>();
        decorations.add("IcingFondant");

        lessSugarCheeseC =  cakeService.createCake("Cheese Cake", "less sugar");
        cake = new CakeModel(lessSugarCheeseC.getName(), lessSugarCheeseC.getType(), lessSugarCheeseC.getPrice());

        cake = cakeDecoratorService.addDecoration(cake, decorations);
    }

    @Test
    public void testIcingFondantDecoratorIsConcreteClass() {
        assertFalse(Modifier.isAbstract(icingFondantDecoratorClass.getModifiers()));
    }

    @Test
    public void testIcingFondantDecoratorIsCakeDecorator() {
        Class<?> parentClass = icingFondantDecoratorClass.getSuperclass();

        assertEquals("project.cakegeeks.cakedecoration.model.CakeDecoratorModel",
                parentClass.getName());
    }

    @Test
    public void testIcingFondantDecoratorOverrideGetIdMethod() throws Exception {
        Method getId = icingFondantDecoratorClass.getDeclaredMethod("getId");

        assertTrue(Modifier.isPublic(getId.getModifiers()));
        assertEquals(0, getId.getParameterCount());

        assertEquals(0, cake.getId());
    }

    @Test
    public void testIcingFondantDecoratorOverrideGetNameMethod() throws Exception {
        Method getName = icingFondantDecoratorClass.getDeclaredMethod("getName");

        assertTrue(Modifier.isPublic(getName.getModifiers()));
        assertEquals(0, getName.getParameterCount());

        assertEquals("Icing Fondant Less Sugar Cheese Cake", cake.getName());
    }

    @Test
    public void testIcingFondantDecoratorOverrideGetPriceMethod() throws Exception {
        Method getPrice = icingFondantDecoratorClass.getDeclaredMethod("getPrice");

        assertTrue(Modifier.isPublic(getPrice.getModifiers()));
        assertEquals(0, getPrice.getParameterCount());

        assertEquals(108000, cake.getPrice());
    }

    @Test
    public void testIcingFondantDecoratorOverrideGetTypeMethod() throws Exception {
        Method getType = icingFondantDecoratorClass.getDeclaredMethod("getType");

        assertTrue(Modifier.isPublic(getType.getModifiers()));
        assertEquals(0, getType.getParameterCount());

        assertEquals("Less Sugar Cheese Cake + Icing Fondant", cake.getType());
    }
}
