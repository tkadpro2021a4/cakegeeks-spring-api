package project.cakegeeks.cakedecoration.model;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CakeDecoratorModelTest {
    private Class<?> cakeDecoratorModelClass;

    @BeforeEach
    public void setUp() throws Exception {
        cakeDecoratorModelClass = Class.forName("project.cakegeeks.cakedecoration.model.CakeDecoratorModel");
    }

    @Test
    public void testCakeDecoratorModelIsAbstract() {
        assertTrue(Modifier.isAbstract(cakeDecoratorModelClass.getModifiers()));
    }

    @Test
    public void testCakeDecoratorModelHasGetCakeMethod() throws Exception {
        Method attack = cakeDecoratorModelClass.getDeclaredMethod("getCake");

        assertTrue(Modifier.isPublic(attack.getModifiers()));
        assertEquals(0, attack.getParameterCount());
        assertEquals("project.cakegeeks.cakecreation.model.CakeModel", attack.getGenericReturnType().getTypeName());
    }

}
