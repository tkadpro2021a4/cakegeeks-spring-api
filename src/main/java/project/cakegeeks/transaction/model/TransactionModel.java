package project.cakegeeks.transaction.model;

import project.cakegeeks.transaction.core.EndState;
import project.cakegeeks.transaction.core.StartState;
import project.cakegeeks.transaction.core.TransactionState;

import project.cakegeeks.cakecreation.model.CakeModel;

public class TransactionModel {
    private CakeModel cake;
    private TransactionState transactionState;

    public TransactionModel(CakeModel cake) {
        this.cake = cake;
        this.transactionState = new StartState();
    }

    public void setTransactionState(TransactionState transactionState) {
        this.transactionState = transactionState;
    }

    public void nextState() {
        if (this.transactionState.tostring().equals("Start State")) {
            this.transactionState = new EndState();
        }
    }

    public String getCakeName() {
        return this.cake.getName();
    }

    public String getCakeType() {
        return this.cake.getType();
    }

    public long getCakeId() {
        return this.cake.getId();
    }

    public int getCakePrice() {
        return  this.cake.getPrice();
    }

    public TransactionState getTransactionState() {
        return this.transactionState;
    }
}
