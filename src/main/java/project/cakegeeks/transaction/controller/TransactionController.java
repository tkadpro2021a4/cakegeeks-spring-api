package project.cakegeeks.transaction.controller;

import project.cakegeeks.transaction.model.TransactionModel;
import project.cakegeeks.transaction.service.TransactionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import project.cakegeeks.cakecreation.service.CakeService;

@Controller
@RequestMapping(path = "/transaction")
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private CakeService cakeService;
    private TransactionModel transactionModel;
    private String state = "start";

    @GetMapping(path = "/{id}/order")
    public  String getTransaction(Model model,@PathVariable(value = "id") long id) {
        transactionModel = transactionService.makeTransaction(cakeService.findCake(id).get());
        if (this.state.equals("end")) {
            transactionModel.nextState();
            this.state = "start";
        }
        model.addAttribute("cake_name", transactionModel.getCakeName());
        model.addAttribute("cake_price", transactionModel.getCakePrice());
        model.addAttribute("state", transactionModel.getTransactionState().tostring());
        return "transaction/transaction";
    }

    @PostMapping("/ordersuccess")
    public String processingTransaction() {
        long id = this.transactionModel.getCakeId();
        this.state = "end";
        return "redirect:/transaction/" + id + "/order";
    }
}
