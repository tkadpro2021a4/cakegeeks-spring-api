package project.cakegeeks.transaction.service;

import project.cakegeeks.transaction.model.TransactionModel;

import org.springframework.stereotype.Service;
import project.cakegeeks.cakecreation.model.CakeModel;

@Service
public class TransactionServiceImpl implements TransactionService {

    @Override
    public TransactionModel makeTransaction(CakeModel cake) {
        return new TransactionModel(cake);
    }
}
