package project.cakegeeks.transaction.service;

import project.cakegeeks.transaction.model.TransactionModel;

import project.cakegeeks.cakecreation.model.CakeModel;

public interface TransactionService {
    TransactionModel makeTransaction(CakeModel cake);
}
