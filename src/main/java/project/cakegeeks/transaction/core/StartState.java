package project.cakegeeks.transaction.core;

public class StartState implements  TransactionState {

    @Override
    public String tostring() {
        return "Start State";
    }
}
