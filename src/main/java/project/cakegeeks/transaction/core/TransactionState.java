package project.cakegeeks.transaction.core;

public interface TransactionState {
    String tostring();
}
