package project.cakegeeks.transaction.core;

public class EndState implements TransactionState {

    @Override
    public String tostring() {
        return "Pesanan Telah Siap";
    }
}
