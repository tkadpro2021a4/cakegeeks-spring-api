package project.cakegeeks.auth.service;

import project.cakegeeks.auth.core.exception.AuthenticationException;
import project.cakegeeks.auth.core.validator.LoginUserValidator;
import project.cakegeeks.auth.core.validator.RegisterUserValidator;
import project.cakegeeks.auth.core.validator.UserValidator;
import project.cakegeeks.auth.repository.UserRepository;
import project.cakegeeks.auth.util.Jwt;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private Jwt jwt;

    @PostConstruct
    private void init() {
        UserValidator.setUserRepository(userRepository);
    }

    /**
     * register user by using register validator class.
     * @param registerUserValidator pre register data capture from user input
     * @return map response object
     * @throws AuthenticationException if authentication exception occur
     */
    public Map<String, Object> register(RegisterUserValidator registerUserValidator) throws AuthenticationException {
        String username = registerUserValidator.getUsername();

        // Step 1: Check input validity and register user
        registerUserValidator.register();

        // Step 2: Create response
        Map<String, Object> response = new HashMap<>();
        var optionalUser = userRepository.findByUsername(username);

        if (optionalUser.isPresent()) {
            var user = optionalUser.get();
            response.put("username", user.getUsername());
            response.put("email", user.getEmail());
            response.put("hashedPassword", user.getPassword());
        }

        return response;
    }

    /**
     * login user by using login validator class.
     * @param loginUserValidator pre login data capture from user input
     * @return map response object
     * @throws AuthenticationException if authentication exception occur
     */
    public Map<String, Object> login(LoginUserValidator loginUserValidator) throws AuthenticationException {
        String username = loginUserValidator.getUsername();

        // Step 1: Check input validity and login user
        loginUserValidator.login();

        // Step 2: Create JWT token
        String jwtToken = jwt.createToken(username);

        // Step 3: Create response
        Map<String, Object> response = new HashMap<>();
        response.put("username", username);
        response.put("Authorization", jwtToken);

        return response;
    }
}
