package project.cakegeeks.auth.service;

import project.cakegeeks.auth.core.exception.AuthenticationException;
import project.cakegeeks.auth.core.validator.LoginUserValidator;
import project.cakegeeks.auth.core.validator.RegisterUserValidator;

import java.util.Map;

public interface UserService {

    public Map<String, Object> register(RegisterUserValidator registerUserValidator) throws AuthenticationException;

    public Map<String, Object> login(LoginUserValidator loginUserValidator) throws AuthenticationException;
}
