package project.cakegeeks.auth.util;

import project.cakegeeks.auth.core.exception.AuthorizationException;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class JwtAuthorization {
    private String jwtToken;

    @Autowired
    private final Jwt jwt;

    public JwtAuthorization(Jwt jwt) {
        this.jwt = jwt;
    }

    public void authorize(String authorization) throws AuthorizationException {
        try {
            String[] tempHeader = authorization.split(" ");
            String tempToken = tempHeader[1];

            boolean tokenValidity = jwt.validateToken(tempToken);
            if (!tokenValidity) {
                throw new AuthorizationException("BAD_REQUEST_TOKEN");
            }
            setJwtToken(tempToken);
        } catch (Exception e) {
            throw new AuthorizationException("BAD_REQUEST_TOKEN");
        }
    }

    /**
     * get absolute user's username from current token.
     * @return String username from current token
     */
    public String getUsername() {
        return jwt.getUsernameFromToken(jwtToken);
    }

    public String getJwtToken() {
        return jwtToken;
    }

    private void setJwtToken(String jwtToken) {
        this.jwtToken = jwtToken;
    }

    /**
     * refresh token and create response.
     * @return Map instance
     */
    public Map<String, Object> getResponse() {
        // Step 1: Refresh token
        refreshToken();

        // Step 2: Create response object
        Map<String, Object> response = new HashMap<>();
        response.put("Authorization", jwtToken);

        return response;
    }

    private void refreshToken() {
        String token = getJwtToken();
        this.jwtToken = jwt.refreshToken(token);
    }
}
