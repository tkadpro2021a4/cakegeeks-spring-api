package project.cakegeeks.auth.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SignatureException;
import java.io.Serializable;
import java.security.Key;
import java.util.Date;
import java.util.function.Function;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Jwt implements Serializable {

    /**
     * Auto-generated serial version ID.
     */
    private static final long serialVersionUID = -7146376590601168500L;

    // 180 minutes
    private static final long JWT_TOKEN_VALIDITY = 3 * 60 * 60 * 1000L;

    private String secretKey;

    @Autowired
    public Jwt(@Value("${jwt.secretKey}") String secretKey) {
        this.secretKey = secretKey;
    }

    // retrieve username from jwt token
    public String getUsernameFromToken(String token) throws SignatureException,
            ExpiredJwtException, MalformedJwtException {
        return getClaimFromToken(token, Claims::getSubject);
    }

    // retrieve expiration date from jwt token
    public Date getExpirationDateFromToken(String token) throws SignatureException,
            ExpiredJwtException, MalformedJwtException {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public <T> T getClaimFromToken(
            String token,
            Function<Claims, T> claimsResolver
    ) throws SignatureException, ExpiredJwtException, MalformedJwtException {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    // for retrieving any information from token we will need the secret key
    private Claims getAllClaimsFromToken(String token) throws SignatureException,
            ExpiredJwtException, MalformedJwtException {
        Key key = Keys.hmacShaKeyFor(secretKey.getBytes());
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    // check if the token has expired
    private Boolean isTokenExpired(String token) throws SignatureException,
            MalformedJwtException {
        try {
            final Date expiration = getExpirationDateFromToken(token);
            return expiration.before(new Date());
        } catch (ExpiredJwtException e) {
            return true;
        }
    }

    /**
     * generate JWT token with given username.
     * @param username username for token
     * @return String the JWT token generated from username
     */
    public String createToken(String username) {
        Claims claims = Jwts.claims().setSubject(username);
        Date now = new Date();
        Date validity = new Date(now.getTime() + JWT_TOKEN_VALIDITY);
        Key key = Keys.hmacShaKeyFor(secretKey.getBytes());

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(validity)
                .signWith(key)
                .compact();
    }

    public String refreshToken(String token) throws SignatureException,
            ExpiredJwtException, MalformedJwtException {
        String username = getUsernameFromToken(token);
        return createToken(username);
    }

    public Boolean validateToken(String token) throws SignatureException,
            ExpiredJwtException, MalformedJwtException {
        return !isTokenExpired(token);
    }
}
