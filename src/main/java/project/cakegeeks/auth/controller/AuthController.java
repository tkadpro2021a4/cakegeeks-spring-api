package project.cakegeeks.auth.controller;

import project.cakegeeks.auth.core.exception.AuthenticationException;
import project.cakegeeks.auth.core.validator.LoginUserValidator;
import project.cakegeeks.auth.core.validator.RegisterUserValidator;
import project.cakegeeks.auth.service.UserService;
import project.cakegeeks.auth.util.JwtAuthorization;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = {"http://localhost:63343",  "https://cakegeeks.herokuapp.com"}, allowedHeaders = {"*"})
@RequestMapping(path = "/auth")
public class AuthController {
    private static final String MESSAGE_STR = "message";

    @Autowired
    private UserService userService;

    @PostMapping(path = "/register", produces = "application/json", consumes = "application/json")
    @ResponseBody
    public ResponseEntity<Map<String, Object>> register(@RequestBody RegisterUserValidator registerUserValidator) {
        try {
            Map<String, Object> response = userService.register(registerUserValidator);
            return ResponseEntity.status(HttpStatus.OK).body(response);
        } catch (AuthenticationException e) {
            Map<String, Object> response = new HashMap<>();
            response.put("error", "REGISTER_ERROR");
            response.put(MESSAGE_STR, e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
    }

    @PostMapping(path = "/login", produces = "application/json")
    @ResponseBody
    public ResponseEntity<Map<String, Object>> login(@RequestBody LoginUserValidator loginUserValidator) {
        try {
            Map<String, Object> response = userService.login(loginUserValidator);
            return ResponseEntity.status(HttpStatus.OK).body(response);
        } catch (AuthenticationException e) {
            Map<String, Object> response = new HashMap<>();
            response.put("error", "LOGIN_ERROR");
            response.put(MESSAGE_STR, e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
    }

    @GetMapping(path = "/authorize", produces = "application/json")
    @ResponseBody
    public ResponseEntity<Map<String, Object>> authorize(@RequestHeader(name = "Authorization", required = false)
                                                                     JwtAuthorization jwtAuthorization) {
        Map<String, Object> response = jwtAuthorization.getResponse();
        response.put("username", jwtAuthorization.getUsername());
        response.put(MESSAGE_STR, "VALID_TOKEN");
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
}
