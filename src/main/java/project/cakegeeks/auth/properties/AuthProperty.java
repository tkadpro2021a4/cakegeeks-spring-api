package project.cakegeeks.auth.properties;

import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.lang.NonNull;
import org.springframework.validation.annotation.Validated;

@Setter
@NoArgsConstructor
@ConfigurationProperties(prefix = "jwt")
@Validated
public class AuthProperty {

    @NonNull
    private String secretKey;
}
