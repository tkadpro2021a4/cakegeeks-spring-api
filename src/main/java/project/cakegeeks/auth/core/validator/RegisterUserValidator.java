package project.cakegeeks.auth.core.validator;

import project.cakegeeks.auth.core.exception.AuthenticationException;
import project.cakegeeks.auth.core.validation.EmailValidation;
import project.cakegeeks.auth.core.validation.PasswordValidation;
import project.cakegeeks.auth.core.validation.UsernameValidation;
import project.cakegeeks.auth.core.validation.Validation;
import project.cakegeeks.auth.model.User;

import at.favre.lib.crypto.bcrypt.BCrypt;

public class RegisterUserValidator extends UserValidator {
    private String email;
    private String verifyPassword;

    public RegisterUserValidator(String username, String email, String password, String verifyPassword) {
        super(username, password);
        this.email = email;
        this.verifyPassword = verifyPassword;
    }

    public void register() throws AuthenticationException {
        isValid();
        save();
    }

    private void isValid() throws AuthenticationException {
        Validation usernameValidation = new UsernameValidation(this);
        Validation emailValidation = new EmailValidation(this);
        Validation passwordValidation = new PasswordValidation(this);
        usernameValidation.setNextHandler(emailValidation);
        emailValidation.setNextHandler(passwordValidation);

        usernameValidation.validate();
    }

    private void save() throws AuthenticationException {

        // Step 1: Check username availability
        if (userRepository.findByUsername(username).isPresent()) {
            throw new AuthenticationException("USERNAME_ALREADY_EXISTS");
        }

        // Step 2: Check email availability
        if (userRepository.findByEmail(email).isPresent()) {
            throw new AuthenticationException("EMAIL_ALREADY_EXISTS");
        }

        // Step 3: Check second password
        if (!secondPasswordValidity()) {
            throw new AuthenticationException("PASSWORD_DOES_NOT_MATCH");
        }

        String tempUsername = username;
        String tempEmail = email;

        // Step 4: Hash password
        var tempPassword = BCrypt.withDefaults().hashToString(PASSWORD_SALT, password.toCharArray());

        // Step 5: Save new user
        var user = new User();
        user.setUsername(tempUsername);
        user.setEmail(tempEmail);
        user.setPassword(tempPassword);
        userRepository.save(user);
    }

    private boolean secondPasswordValidity() {
        return password.equals(verifyPassword);
    }

    @Override
    public String getEmail() {
        return email;
    }
}
