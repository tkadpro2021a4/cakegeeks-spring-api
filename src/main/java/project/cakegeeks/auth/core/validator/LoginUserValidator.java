package project.cakegeeks.auth.core.validator;

import project.cakegeeks.auth.core.exception.AuthenticationException;
import project.cakegeeks.auth.model.User;

import at.favre.lib.crypto.bcrypt.BCrypt;
import java.util.Optional;

public class LoginUserValidator extends UserValidator {
    public LoginUserValidator(String username, String password) {
        super(username, password);
    }

    public void login() throws AuthenticationException {
        isValid();
    }

    private void isValid() throws AuthenticationException {
        Optional<User> tempUser = userRepository.findByUsername(username);

        // Step 1: Check username existence
        if (tempUser.isEmpty()) {
            throw new AuthenticationException("WRONG_USERNAME_OR_PASSWORD");
        }

        // Step 2: Check password correctness
        User user = tempUser.get();
        boolean passwordCorrectness = BCrypt.verifyer().verify(password.toCharArray(), user.getPassword()).verified;
        if (!passwordCorrectness) {
            throw new AuthenticationException("WRONG_USERNAME_OR_PASSWORD");
        }
    }
}
