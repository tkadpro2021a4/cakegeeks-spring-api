package project.cakegeeks.auth.core.validator;

import project.cakegeeks.auth.repository.UserRepository;

public abstract class UserValidator {
    protected String username;
    protected String password;

    protected static final int PASSWORD_SALT = 7;

    protected static UserRepository userRepository;

    protected UserValidator(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return null;
    }

    public String getPassword() {
        return password;
    }

    public static void setUserRepository(UserRepository userRepository) {
        UserValidator.userRepository = userRepository;
    }
}
