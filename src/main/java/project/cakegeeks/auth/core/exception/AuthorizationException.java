package project.cakegeeks.auth.core.exception;

public class AuthorizationException extends Exception {
    public AuthorizationException(String message) {
        super(message);
    }
}
