package project.cakegeeks.auth.core.validation;

import project.cakegeeks.auth.core.exception.AuthenticationException;
import project.cakegeeks.auth.core.validator.UserValidator;

import java.util.regex.Pattern;

public class EmailValidation implements Validation {

    private final Pattern emailPattern =
            Pattern.compile("^([\\w-\\.]+){1,64}@([\\w&&[^_]]+){2,255}.[a-z]{2,}$");

    private static final int EMAIL_MIN_LENGTH = 8;
    private static final int EMAIL_MAX_LENGTH = 50;

    private final UserValidator userValidator;
    private Validation nextHandler;

    public EmailValidation(UserValidator userValidator) {
        this.userValidator = userValidator;
    }

    @Override
    public void validate() throws AuthenticationException {
        String email = userValidator.getEmail();
        if (email.length() < EMAIL_MIN_LENGTH || email.length() > EMAIL_MAX_LENGTH) {
            throw new AuthenticationException("EMAIL_LENGTH_NOT_SUPPORTED");
        }
        if (!emailPattern.matcher(email).find()) {
            throw new AuthenticationException("INVALID_EMAIL");
        }
        if (nextHandler != null) {
            nextHandler.validate();
        }
    }

    public void setNextHandler(Validation nextHandler) {
        this.nextHandler = nextHandler;
    }
}
