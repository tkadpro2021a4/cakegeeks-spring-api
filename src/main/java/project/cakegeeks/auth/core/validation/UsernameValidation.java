package project.cakegeeks.auth.core.validation;

import project.cakegeeks.auth.core.exception.AuthenticationException;
import project.cakegeeks.auth.core.validator.UserValidator;

import java.util.regex.Pattern;

public class UsernameValidation implements Validation {

    private final Pattern usernamePattern = Pattern.compile("^[a-z][a-z0-9]{2,48}$");

    private static final int USERNAME_MIN_LENGTH = 3;
    private static final int USERNAME_MAX_LENGTH = 50;

    private final UserValidator userValidator;
    private Validation nextHandler;

    public UsernameValidation(UserValidator userValidator) {
        this.userValidator = userValidator;
    }

    @Override
    public void validate() throws AuthenticationException {
        String username = userValidator.getUsername();
        if (username == null || username.length() < USERNAME_MIN_LENGTH
                || username.length() > USERNAME_MAX_LENGTH) {
            throw new AuthenticationException("USERNAME_LENGTH_NOT_SUPPORTED");
        }
        if (!usernamePattern.matcher(username).find()) {
            throw new AuthenticationException("USERNAME_CAN_ONLY_CONTAIN_LOWERCASE_AND_NUMBER" +
                    "_AND_MUST_BEGIN_WITH_LETTER");
        }
        if (nextHandler != null) {
            nextHandler.validate();
        }
    }

    public void setNextHandler(Validation nextHandler) {
        this.nextHandler = nextHandler;
    }
}
