package project.cakegeeks.auth.core.validation;

import project.cakegeeks.auth.core.exception.AuthenticationException;
import project.cakegeeks.auth.core.validator.UserValidator;

import java.util.regex.Pattern;

public class PasswordValidation implements Validation {

    private final Pattern passwordPattern = Pattern.compile("^(?=.*[A-Z])(?=.*[a-z])" +
            "(?=.*[0-9])(?=.*[#?!@$ %^&*-]).{8,}$");

    private static final int PASSWORD_MAX_LEN = 50;

    UserValidator userValidator;
    Validation nextHandler;

    public PasswordValidation(UserValidator userValidator) {
        this.userValidator = userValidator;
    }

    @Override
    public void validate() throws AuthenticationException {
        String password = userValidator.getPassword();
        if (password.length() > PASSWORD_MAX_LEN) {
            throw new AuthenticationException("PASSWORD_CANNOT_BE_LONGER_THAN_50_CHARACTER");
        }
        if (!passwordPattern.matcher(password).find()) {
            throw new AuthenticationException("PASSWORD_MUST_AT_LEAST_CONTAIN_8_CHARACTER" +
                    "1_UPPERCASE_1_NUMBER_AND_1_SYMBOL");
        }
        if (nextHandler != null) {
            nextHandler.validate();
        }
    }


    public void setNextHandler(Validation nextHandler) {
        this.nextHandler = nextHandler;
    }
}
