package project.cakegeeks.auth.core.validation;

import project.cakegeeks.auth.core.exception.AuthenticationException;

public interface Validation {

    public void validate() throws AuthenticationException;

    public void setNextHandler(Validation validation);
}
