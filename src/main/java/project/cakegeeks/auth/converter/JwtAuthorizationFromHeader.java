package project.cakegeeks.auth.converter;

import project.cakegeeks.auth.core.exception.AuthorizationException;
import project.cakegeeks.auth.util.Jwt;
import project.cakegeeks.auth.util.JwtAuthorization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

@Component
public class JwtAuthorizationFromHeader implements Converter<String, JwtAuthorization> {

    @Autowired
    private Jwt jwt;

    private JwtAuthorization jwtAuthorization;

    public JwtAuthorizationFromHeader() {

    }

    public JwtAuthorizationFromHeader(Jwt jwt) {
        this.jwt = jwt;
    }

    @Override
    public JwtAuthorization convert(String authorization) {
        try {
            jwtAuthorization = new JwtAuthorization(jwt);
            jwtAuthorization.authorize(authorization);
            return jwtAuthorization;
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }
}
