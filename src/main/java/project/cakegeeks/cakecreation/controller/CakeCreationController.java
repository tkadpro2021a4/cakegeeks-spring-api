package project.cakegeeks.cakecreation.controller;

import project.cakegeeks.cakecreation.core.Cake;
import project.cakegeeks.cakecreation.model.CakeModel;
import project.cakegeeks.cakecreation.service.CakeService;

import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "")
public class CakeCreationController {
    @Autowired
    private CakeService cakeService;

    @RequestMapping(path = "/addcake", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public String chooseCake() {
        return "creation/addcake";
    }

    @PostMapping(path = "/nextstep")
    public ResponseEntity nextStep(@RequestBody ObjectNode objectNode) {
        String base = objectNode.get("cake").asText();
        String custom = objectNode.get("custom").asText();
        Cake cake = cakeService.createCake(base, custom);
        CakeModel cakeModel = cakeService.addCake(cake);
        return ResponseEntity.ok(cakeModel);
    }
}
