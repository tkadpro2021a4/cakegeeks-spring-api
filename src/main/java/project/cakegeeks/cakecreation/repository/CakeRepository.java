package project.cakegeeks.cakecreation.repository;

import project.cakegeeks.cakecreation.model.CakeModel;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CakeRepository extends JpaRepository<CakeModel,Long> {
}
