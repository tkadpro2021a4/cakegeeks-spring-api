package project.cakegeeks.cakecreation.core;

public class OgFactory implements CakeFactory {
    @Override
    public CheeseCake createCheeseC(String name, int basePrice) {
        return new OgCheeseC(name, basePrice);
    }

    @Override
    public ChiffonCake createChiffonC(String name, int basePrice) {
        return new OgChiffonC(name, basePrice);
    }
}
