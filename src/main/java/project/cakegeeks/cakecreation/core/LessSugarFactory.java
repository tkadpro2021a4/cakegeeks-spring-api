package project.cakegeeks.cakecreation.core;

public class LessSugarFactory implements CakeFactory {
    @Override
    public CheeseCake createCheeseC(String name, int basePrice) {
        return new LessSugarCheeseC(name, basePrice);
    }

    @Override
    public ChiffonCake createChiffonC(String name, int basePrice) {
        return new LessSugarChiffonC(name, basePrice);
    }
}
