package project.cakegeeks.cakecreation.core;

public class OgChiffonC implements ChiffonCake {
    private String name;
    private int basePrice;
    private String type = "OG";

    public OgChiffonC(String name, int basePrice) {
        this.name = name;
        this.basePrice = basePrice;
    }

    @Override
    public String getName() {
        return "Chiffon Cake";
    }

    @Override
    public int getPrice() {
        return basePrice;
    }

    @Override
    public String getType() {
        return "Chiffon Cake";
    }
}
