package project.cakegeeks.cakecreation.core;

public class OgCheeseC implements CheeseCake {
    private String name;
    private int basePrice;
    private String type = "OG";

    public OgCheeseC(String name, int basePrice) {
        this.name = name;
        this.basePrice = basePrice;
    }

    @Override
    public String getName() {
        return "Cheese Cake";
    }

    @Override
    public int getPrice() {
        return basePrice;
    }

    @Override
    public String getType() {
        return "Cheese Cake";
    }

}
