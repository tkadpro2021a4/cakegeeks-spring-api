package project.cakegeeks.cakecreation.core;

public class LessSugarCheeseC implements CheeseCake {
    private String name;
    private int basePrice;
    private String type = "Less";

    public LessSugarCheeseC(String name, int basePrice) {
        this.name = name;
        this.basePrice = basePrice;
    }

    @Override
    public String getName() {
        return "Less Sugar Cheese Cake";
    }

    @Override
    public int getPrice() {
        return basePrice - 2000;
    }

    @Override
    public String getType() {
        return "Less Sugar Cheese Cake";
    }
}
