package project.cakegeeks.cakecreation.core;

public interface CakeFactory {
    public CheeseCake createCheeseC(String name, int basePrice);

    public ChiffonCake createChiffonC(String name, int basePrice);
}
