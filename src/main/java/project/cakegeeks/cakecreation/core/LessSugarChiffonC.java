package project.cakegeeks.cakecreation.core;

public class LessSugarChiffonC implements ChiffonCake {
    private String name;
    private int basePrice;
    private String type = "Less";

    public LessSugarChiffonC(String name, int basePrice) {
        this.name = name;
        this.basePrice = basePrice;
    }

    @Override
    public String getName() {
        return "Less Sugar Chiffon Cake";
    }

    @Override
    public int getPrice() {
        return basePrice - 2000;
    }

    @Override
    public String getType() {
        return "Less Sugar Chiffon Cake";
    }
}