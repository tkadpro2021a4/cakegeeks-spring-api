package project.cakegeeks.cakecreation.core;

public interface Cake {
    String getName();

    int getPrice();

    String getType();
}
