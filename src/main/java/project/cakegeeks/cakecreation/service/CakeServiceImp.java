package project.cakegeeks.cakecreation.service;

import project.cakegeeks.cakecreation.core.Cake;
import project.cakegeeks.cakecreation.core.CakeFactory;
import project.cakegeeks.cakecreation.core.LessSugarFactory;
import project.cakegeeks.cakecreation.core.OgFactory;
import project.cakegeeks.cakecreation.model.CakeModel;
import project.cakegeeks.cakecreation.repository.CakeRepository;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CakeServiceImp implements CakeService {
    @Autowired
    private CakeRepository cakeRepository;

    @Override
    public List<CakeModel> findAll() {
        return cakeRepository.findAll();
    }

    @Override
    public Optional<CakeModel> findCake(Long id) {
        return cakeRepository.findById(id);
    }

    @Override
    public void erase(Long id) {
        cakeRepository.deleteById(id);
    }

    @Override
    public CakeModel register(CakeModel cake) {
        cakeRepository.save(cake);
        return cake;
    }

    @Override
    public CakeModel rewrite(CakeModel cake) {
        CakeModel cakes = findCake(cake.getId()).get();
        cakes.setName(cake.getName());
        cakes.setType(cake.getType());
        cakes.setPrice(cake.getPrice());
        cakeRepository.save(cakes);
        return cake;
    }

    @Override
    public Cake createCake(String base, String baseChoice) {
        Cake cake;
        CakeFactory cakeFactory;
        if (baseChoice.equalsIgnoreCase("less sugar")) {
            cakeFactory = new LessSugarFactory();
        } else {
            cakeFactory = new OgFactory();
        }
        if (base.equalsIgnoreCase("cheese cake")) {
            cake = cakeFactory.createCheeseC("Cheese Cake", 75000);
        } else {
            cake = cakeFactory.createChiffonC("Chiffon Cake", 70000);
        }
        return cake;
    }

    @Override
    public CakeModel addCake(Cake cake) {
        CakeModel cakeSave = new CakeModel(
                cake.getName(),
                cake.getType(),
                cake.getPrice()
        );
        return cakeRepository.save(cakeSave);
    }
}
