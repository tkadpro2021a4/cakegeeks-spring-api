package project.cakegeeks.cakecreation.service;

import project.cakegeeks.cakecreation.core.Cake;
import project.cakegeeks.cakecreation.model.CakeModel;

import java.util.List;
import java.util.Optional;

public interface CakeService {
    public List<CakeModel> findAll();

    public Optional<CakeModel> findCake(Long id);

    public void erase(Long id);

    public CakeModel register(CakeModel cake);

    public CakeModel rewrite(CakeModel cake);

    public Cake createCake(String base, String baseChoice);

    public CakeModel addCake(Cake cake);
}
