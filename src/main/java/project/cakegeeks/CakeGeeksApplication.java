package project.cakegeeks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import project.cakegeeks.auth.properties.AuthProperty;

@SpringBootApplication
@EnableConfigurationProperties(AuthProperty.class)
public class CakeGeeksApplication {

	public static void main(String[] args) {
		SpringApplication.run(CakeGeeksApplication.class, args);
	}

}
