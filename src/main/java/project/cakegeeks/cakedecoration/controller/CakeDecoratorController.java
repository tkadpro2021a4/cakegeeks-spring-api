package project.cakegeeks.cakedecoration.controller;

import project.cakegeeks.cakedecoration.service.CakeDecoratorService;

import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import project.cakegeeks.cakecreation.model.CakeModel;
import project.cakegeeks.cakecreation.service.CakeService;

@Controller
@RequestMapping(path = "")
public class CakeDecoratorController {

    @Autowired
    private CakeDecoratorService cakeDecoratorService;

    @Autowired
    private CakeService cakeService;

    @RequestMapping(path = "{id}/addTopping", method = RequestMethod.GET)
    public String getCake(@PathVariable(value = "id") long id, Model model)  {
        model.addAttribute("cake", cakeService.findCake(id).get());
        return "addTopping";
    }

    @PostMapping(path = "{id}/addTopping")
    public ResponseEntity addTopping(@PathVariable(value = "id") long id, @RequestBody ObjectNode objectNode) {
        String toppings = objectNode.get("toppings").asText();
        CakeModel cake = cakeService.findCake(id).get();
        List<String> listTopping = new ArrayList<>(Arrays.asList(toppings.split(" ")));
        cake = cakeDecoratorService.addDecoration(cake, listTopping);
        cake = cakeService.rewrite(cake);
        return ResponseEntity.ok(cake);
    }
}
