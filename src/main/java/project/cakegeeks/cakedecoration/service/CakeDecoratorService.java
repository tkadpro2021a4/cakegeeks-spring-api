package project.cakegeeks.cakedecoration.service;

import java.util.List;
import project.cakegeeks.cakecreation.model.CakeModel;

public interface CakeDecoratorService {
    CakeModel addDecoration(CakeModel cake, List<String> decoration);
}
