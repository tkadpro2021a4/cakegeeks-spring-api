package project.cakegeeks.cakedecoration.service;

import project.cakegeeks.cakedecoration.core.AlmondDecorator;
import project.cakegeeks.cakedecoration.core.CandyDecorator;
import project.cakegeeks.cakedecoration.core.CheeseDecorator;
import project.cakegeeks.cakedecoration.core.FruitDecorator;
import project.cakegeeks.cakedecoration.core.IcingFondantDecorator;
import project.cakegeeks.cakedecoration.core.WhippedCreamDecorator;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.cakegeeks.cakecreation.model.CakeModel;
import project.cakegeeks.cakecreation.service.CakeService;

@Service
public class CakeDecoratorServiceImpl implements CakeDecoratorService {

    @Autowired
    private CakeService cakeService;

    public CakeModel addDecoration(CakeModel cakes, List<String> decorations) {

        for (String decoration : decorations) {
            if (decoration.equals("Almond")) {
                cakes = new AlmondDecorator(cakes);
            } else if (decoration.equals("Fruits")) {
                cakes = new FruitDecorator(cakes);
            } else if (decoration.equals("Candy")) {
                cakes = new CandyDecorator(cakes);
            } else if (decoration.equals("IcingFondant")) {
                cakes = new IcingFondantDecorator(cakes);
            } else if (decoration.equals("WhippedCream")) {
                cakes = new WhippedCreamDecorator(cakes);
            } else if (decoration.equals("Cheese")) {
                cakes = new CheeseDecorator(cakes);
            }
        }
        return cakes;
    }

}
