package project.cakegeeks.cakedecoration.core;

import project.cakegeeks.cakedecoration.model.CakeDecoratorModel;

import project.cakegeeks.cakecreation.model.CakeModel;

public class CandyDecorator extends CakeDecoratorModel {

    public CandyDecorator(CakeModel cake) {
        super(cake);
    }

    @Override
    public long getId() {
        return cakes.getId();
    }

    @Override
    public String getName() {
        return "Candy " + cakes.getName();
    }

    @Override
    public int getPrice() {
        return cakes.getPrice() + 30000;
    }

    @Override
    public String getType() {
        return cakes.getType() + " + Candy";
    }

}
