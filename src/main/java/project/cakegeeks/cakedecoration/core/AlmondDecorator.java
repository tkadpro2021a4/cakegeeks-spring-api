package project.cakegeeks.cakedecoration.core;

import project.cakegeeks.cakedecoration.model.CakeDecoratorModel;

import project.cakegeeks.cakecreation.model.CakeModel;

public class AlmondDecorator extends CakeDecoratorModel {

    public AlmondDecorator(CakeModel cakes) {
        super(cakes);
    }

    @Override
    public long getId() {
        return cakes.getId();
    }

    @Override
    public String getName() {
        return "Almond " + cakes.getName();
    }

    @Override
    public int getPrice() {
        return cakes.getPrice() + 18000;
    }

    @Override
    public String getType() {
        return cakes.getType() + " + Almond";
    }
}
