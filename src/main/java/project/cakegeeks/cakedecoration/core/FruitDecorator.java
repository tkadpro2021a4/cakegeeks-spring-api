package project.cakegeeks.cakedecoration.core;

import project.cakegeeks.cakedecoration.model.CakeDecoratorModel;

import project.cakegeeks.cakecreation.model.CakeModel;

public class FruitDecorator extends CakeDecoratorModel {

    public FruitDecorator(CakeModel cake) {
        super(cake);
    }

    @Override
    public long getId() {
        return cakes.getId();
    }

    @Override
    public String getName() {
        return "Fruity " + cakes.getName();
    }

    @Override
    public int getPrice() {
        return cakes.getPrice() + 50000;
    }

    @Override
    public String getType() {
        return cakes.getType() + " + Fruits";
    }

}
