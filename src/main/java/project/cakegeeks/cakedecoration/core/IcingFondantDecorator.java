package project.cakegeeks.cakedecoration.core;

import project.cakegeeks.cakedecoration.model.CakeDecoratorModel;

import project.cakegeeks.cakecreation.model.CakeModel;

public class IcingFondantDecorator extends CakeDecoratorModel {

    public IcingFondantDecorator(CakeModel cake) {
        super(cake);
    }

    @Override
    public long getId() {
        return cakes.getId();
    }

    @Override
    public String getName() {
        return "Icing Fondant " + cakes.getName();
    }

    @Override
    public int getPrice() {
        return cakes.getPrice() + 35000;
    }

    @Override
    public String getType() {
        return cakes.getType() + " + Icing Fondant";
    }

}
