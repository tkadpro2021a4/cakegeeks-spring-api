package project.cakegeeks.cakedecoration.core;

import project.cakegeeks.cakedecoration.model.CakeDecoratorModel;

import project.cakegeeks.cakecreation.model.CakeModel;

public class WhippedCreamDecorator extends CakeDecoratorModel {

    public WhippedCreamDecorator(CakeModel cake) {
        super(cake);
    }

    @Override
    public long getId() {
        return cakes.getId();
    }

    @Override
    public String getName() {
        return "Whipped Cream " + cakes.getName();
    }

    @Override
    public int getPrice() {
        return cakes.getPrice() + 20000;
    }

    @Override
    public String getType() {
        return cakes.getType() + " + Whipped Cream";
    }

}
