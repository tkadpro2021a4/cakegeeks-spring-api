package project.cakegeeks.cakedecoration.core;

import project.cakegeeks.cakedecoration.model.CakeDecoratorModel;

import project.cakegeeks.cakecreation.model.CakeModel;

public class CheeseDecorator extends CakeDecoratorModel {

    public CheeseDecorator(CakeModel cake) {
        super(cake);
    }

    @Override
    public long getId() {
        return cakes.getId();
    }

    @Override
    public String getName() {
        return "Cheese " + cakes.getName();
    }

    @Override
    public int getPrice() {
        return cakes.getPrice() + 25000;
    }

    @Override
    public String getType() {
        return cakes.getType() + " + Cheese";
    }

}
