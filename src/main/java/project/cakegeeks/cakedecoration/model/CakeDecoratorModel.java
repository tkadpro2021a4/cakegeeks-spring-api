package project.cakegeeks.cakedecoration.model;

import project.cakegeeks.cakecreation.model.CakeModel;

public abstract class CakeDecoratorModel extends CakeModel {

    protected CakeModel cakes;

    public CakeDecoratorModel(CakeModel cakes) {
        this.cakes = cakes;
    }

    public CakeModel getCake() {
        return cakes;
    }

}
