package project.cakegeeks.history.controller;

import org.springframework.web.bind.annotation.*;
import project.cakegeeks.auth.util.JwtAuthorization;
import project.cakegeeks.history.model.History;
import project.cakegeeks.history.service.HistoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Map;


@RestController
@RequestMapping(path = "/history")
public class HistoryController {

    @Autowired
    HistoryService historyService;

    @GetMapping(path = "")
    public ResponseEntity<Iterable<History>> getAllUserHistory(
            @RequestHeader(name = "Authorization") JwtAuthorization jwtAuthorization) {
        Iterable<History> response = historyService.findAllTransactionHistoryByUsername(jwtAuthorization.getUsername());
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @PostMapping(path = "")
    public ResponseEntity<History> addHistoryLog(
            @RequestHeader(name = "Authorization") JwtAuthorization jwtAuthorization,
            @RequestBody Map<String, String> request) {
        var history = historyService.addHistoryLog(jwtAuthorization.getUsername(), request.get("cakeName"),
                Integer.parseInt(request.get("price")));
        return ResponseEntity.status(HttpStatus.OK).body(history);
    }
}
