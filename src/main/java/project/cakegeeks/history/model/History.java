package project.cakegeeks.history.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "history")
@NoArgsConstructor
public class History {

    @Getter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Getter
    @Setter
    @Column(name = "username", updatable = false, nullable = false)
    private String username;

    @Getter
    @Setter
    @Column(name = "cakename", nullable = false, columnDefinition = "TEXT")
    private String cakeName;

    @Getter
    @Setter
    @Column(name = "price", nullable = false)
    private int price;

    @Setter
    @Getter
    @Column(name = "date", nullable = false)
    private String time;

}
