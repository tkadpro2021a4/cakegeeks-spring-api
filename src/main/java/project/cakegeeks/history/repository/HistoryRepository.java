package project.cakegeeks.history.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import project.cakegeeks.history.model.History;

public interface HistoryRepository extends JpaRepository<History, Long> {

    Iterable<History> findAllByUsername(String username);

    @Query(
            value = "SELECT coalesce(sum(h.price), 0) FROM extra.history h WHERE h.username = :username",
            nativeQuery = true
    )
    int totalTransactionByUsername(@Param("username") String username);
}
