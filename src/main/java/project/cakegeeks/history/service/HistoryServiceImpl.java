package project.cakegeeks.history.service;

import project.cakegeeks.history.model.History;
import project.cakegeeks.history.repository.HistoryRepository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HistoryServiceImpl implements HistoryService {

    @Autowired
    HistoryRepository historyRepository;

    public History addHistoryLog(String username, String cakeName, int price) {
        var history = new History();
        history.setUsername(username);
        history.setCakeName(cakeName);
        history.setPrice(price);
        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss 'at' dd-MM-yyyy");
        history.setTime(localDateTime.format(dateTimeFormatter));
        historyRepository.save(history);
        return history;
    }

    public Iterable<History> findAllTransactionHistoryByUsername(String username) {
        return historyRepository.findAllByUsername(username);
    }
}
