package project.cakegeeks.history.service;

import project.cakegeeks.history.model.History;

public interface HistoryService {

    History addHistoryLog(String username, String cakeName, int price);

    Iterable<History> findAllTransactionHistoryByUsername(String username);

}
